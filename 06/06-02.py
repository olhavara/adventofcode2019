import logging
from collections import defaultdict as dd

logging.basicConfig(filename='example.log', level=logging.DEBUG)

SEP = ')'


def true(*args, **kwargs):
    return True


def false(*args, **kwargs):
    return False


class Planet:
    def __init__(self, name=None, center=None, orbitals=None, doc=None):
        self.name = name
        self.center = center
        self.orbitals_ = {orbital.name: orbital for orbital in orbitals} if orbitals else dict()
        self.dist_of_centre = doc

    @classmethod
    def from_line(cls, line, orbitals=None):
        planet = cls(name=line[-3:], orbitals=orbitals)
        center = cls(name=line[:3], orbitals=[planet])
        planet.center = center
        return planet

    @property
    def orbitals(self):
        return self.orbitals_.values()

    @property
    def path(self):
        path = [self.name]
        return path if self.is_root() else self.center.path + path

    @property
    def level(self):
        return len(self.path) - 1

    @property
    def root(self):
        return self if self.is_root() else self.parent.root

    @property
    def root_name(self):
        return self.root.name

    def is_root(self):
        return self.center is None

    def is_leaf(self):
        return len(self.orbitals_) == 0

    def planets(self, depth=None, filter_=true, depth_cond=false, from_top=True, sorted_=False,
                **kwargs):
        if depth is None or depth >= 0:
            if from_top and (filter_(self) or depth_cond(depth)):
                yield self
            if not sorted_:
                orbitals = self.orbitals
            else:
                orbitals = sorted(self.orbitals, key=lambda x: x.name if x.name else 'None')
            for orbital in orbitals:
                new_depth = None if depth is None else depth - 1
                yield from orbital.planets(depth=new_depth, filter_=filter_, depth_cond=depth_cond, from_top=from_top,
                                           sorted_=sorted_, **kwargs)
            if not from_top and (filter_(self) or depth_cond(depth)):
                yield self

    def subplanets(self, depth=None, from_top=True, **kwargs):
        for planet in self.orbitals:
            depth_adj = depth - 1 if depth is not None else None
            yield from planet.planets(depth=depth_adj, from_top=from_top, **kwargs)

    def leaves(self, depth=None, from_top=True, **kwargs):
        yield from self.planets(depth=depth, filter_=lambda x: x.is_leaf(), depth_cond=lambda d: d == 0,
                                from_top=from_top, **kwargs)

    def planets_count(self, depth=None, filter_=true, depth_cond=false):
        return suma((1 for _ in self.planets(depth, filter_=filter_, depth_cond=depth_cond)))

    def leaves_count(self, depth=None):
        return suma((1 for _ in self.leaves(depth)))

    def add_orbital(self, planet):
        if planet.name in self.orbitals_.keys():
            self.orbitals_[planet.name].merge(planet)
        else:
            planet.center = self
            self.orbitals_[planet.name] = planet

    def merge(self, another):
        assert self.name == another.name
        for subplanet in another.orbitals:
            self.add_orbital(subplanet)
        return self

    def _is_empty(self, **kwargs):
        return False

    def to_simple_dict(self, **kwargs):
        name = self._simple_dict_name(**kwargs)
        return {
            name: {next(iter(c.to_simple_dict(**kwargs).keys())): next(iter(c.to_simple_dict(**kwargs).values())) for c
                   in
                   self.orbitals}
        }

    def _simple_dict_name(self, **kwargs):
        return self.name


planet_map = dd(list)
f = open('input06.txt', mode='r')
for line in f:
    centre, orbital = line.strip().split(')')
    planet_map[centre].append(orbital)

Base = Planet('COM')


def build(base=Base):
    for name_ in planet_map[base.name]:
        New_planet = Planet(name=name_, center=base.name)
        base.add_orbital(New_planet)
        build(New_planet)


build()

suma = 0
for planet in Base.leaves():
    if planet.name == 'YOU':
        You_path = planet.path
    if planet.name == 'SAN':
        San_path = planet.path

print(You_path)
print(San_path)
while You_path[1] == San_path[1]:
    You_path = You_path[1:]
    San_path = San_path[1:]
print(You_path)
print(San_path)
print(len(You_path) + len(San_path) - 4)
