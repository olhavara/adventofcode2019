import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)

f = open('input05.txt', 'r')
diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 99: 0})

Done = False
OUTPUT = []


def Test(i):
    global Done
    global diag_prog
    instruction = str(diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]
    logging.debug(' opcode: %s, modes: %s', op_code, modes)

    def Mode(modes, _pos, param):
        if not int(modes[param]):
            return diag_prog[diag_prog[_pos + param + 1]]
        else:
            return diag_prog[_pos + param + 1]

    def Add(_modes, pos):
        logging.debug(' Adding:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[diag_prog[pos + 3]] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)

    def Multiply(_modes, pos):
        logging.debug(' Multiplying:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[diag_prog[pos + 3]] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)

    def Read(_modes, pos):
        diag_prog[diag_prog[pos + 1]] = int(input("What is the input:"))

    def Write(_modes, pos):
        OUTPUT.append(Mode(_modes, pos, 0))

    if op_code == 99:
        Done = True
        return 0
    elif op_code == 1:
        Add(modes, i)
        return 4
    elif op_code == 2:
        Multiply(modes, i)
        return 4
    elif op_code == 3:
        Read(modes, i)
        return 2
    elif op_code == 4:
        Write(modes, i)
        return 2


pointer = 0
while not Done:
    posun = Test(pointer)
    pointer += posun

print(OUTPUT[-1])
