import math
from copy import deepcopy as dp

positions = []
with open('input12.txt', 'r') as f:
    for line in f:
        positions.append(list(map(lambda x: int(x.strip('<>xzy= ')), line.strip().split(','))))
original_state = []
for position in positions:
    original_state.append([position, [0, 0, 0]])

planets = dp(original_state)
cartesian = ['x', 'y', 'z']


def step():
    for position in range(len(cartesian)):
        for planet in range(len(planets)):
            for planet_ in range(planet + 1, len(planets)):
                if planets[planet][0][position] < planets[planet_][0][position]:
                    planets[planet][1][position] += 1
                    planets[planet_][1][position] -= 1
                elif planets[planet][0][position] > planets[planet_][0][position]:
                    planets[planet][1][position] -= 1
                    planets[planet_][1][position] += 1
    for position in range(len(cartesian)):
        for planet in range(len(planets)):
            planets[planet][0][position] += planets[planet][1][position]


def energy():
    total_energy = 0
    for planet in range(len(planets)):
        kin_energy = 0
        pot_energy = 0
        for position in range(len(cartesian)):
            pot_energy += abs(planets[planet][0][position])
            kin_energy += abs(planets[planet][1][position])
        total_energy += kin_energy * pot_energy
    return total_energy


def get_primes(integer):
    primes = []
    Numbers = [n for n in range(2, int(integer) + 1)]
    while len(Numbers):
        prime = Numbers.pop(0)
        primes.append(prime)
        i = 0
        while i < len(Numbers):
            if Numbers[i] % prime == 0:
                Numbers.pop(i)
            else:
                i += 1
    return primes


def get_factorization(integer):
    primes = get_primes(math.sqrt(integer))
    factors = []
    if integer == 1:
        return [1]
    else:
        i = 0
        while integer > 1 and i < len(primes):
            if integer % primes[i] == 0:
                factors.append(primes[i])
                integer = integer // primes[i]
            else:
                i += 1
        if integer > 1:
            factors.append(integer)
        return factors


def nsn(int1, int2):
    fac1 = get_factorization(int1)
    fac2 = get_factorization(int2)
    fac = []
    while fac1 and fac2:
        if fac1[0] == fac2[0]:
            fac.append(fac1.pop(0))
            fac2.pop(0)
        elif fac1[0] < fac2[0]:
            fac.append(fac1.pop(0))
        else:
            fac.append(fac2.pop(0))
    fac += fac1
    fac += fac2
    result = 1
    for number in fac:
        result *= number
    return result


total = 1
for position in range(len(cartesian)):
    planets = dp(original_state)
    # print(planets)
    count = 1
    step()
    while not ((planets[0][1][position] == 0) &
               (planets[1][1][position] == 0) &
               (planets[2][1][position] == 0) &
               (planets[3][1][position] == 0) &
               (planets[0][0][position] == original_state[0][0][position]) &
               (planets[1][0][position] == original_state[1][0][position]) &
               (planets[2][0][position] == original_state[2][0][position]) &
               (planets[3][0][position] == original_state[3][0][position])
    ):
        step()
        count += 1
    # print(planets)
    # print(count)
    total = nsn(total, count)
    # print(total)

print(total)
