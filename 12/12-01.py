
positions = []
with open('input12.txt', 'r') as f:
    for line in f:
        positions.append(list(map(lambda x: int(x.strip('<>xzy= ')), line.strip().split(','))))
planets = []
for position in positions:
    planets.append([position, [0, 0, 0]])

cartesian = ['x', 'y', 'z']


def step():
    for position in range(len(cartesian)):
        for planet in range(len(planets)):
            for planet_ in range(planet + 1, len(planets)):
                if planets[planet][0][position] < planets[planet_][0][position]:
                    planets[planet][1][position] += 1
                    planets[planet_][1][position] -= 1
                elif planets[planet][0][position] > planets[planet_][0][position]:
                    planets[planet][1][position] -= 1
                    planets[planet_][1][position] += 1
    for position in range(len(cartesian)):
        for planet in range(len(planets)):
            planets[planet][0][position] += planets[planet][1][position]


def energy():
    total_energy = 0
    for planet in range(len(planets)):
        kin_energy = 0
        pot_energy = 0
        for position in range(len(cartesian)):
            pot_energy += abs(planets[planet][0][position])
            kin_energy += abs(planets[planet][1][position])
        total_energy += kin_energy * pot_energy
    return total_energy


print(planets)
for _ in range(1000):
    step()
print(planets)
print(energy())
