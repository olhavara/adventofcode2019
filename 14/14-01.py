import collections

elements = collections.OrderedDict()
elements['ORE'] = None
leftovers = collections.OrderedDict()
leftovers['ORE'] = None
reactions = [[0]]


def add_element(_reactions, _elements, _elem):
    _elements[_elem] = None
    for row in _reactions:
        row.append(0)
    _reactions.append([0] * len(_elements))
    return _reactions, _elements


def add_reactants(reactions_, elements_, prod_, reac_, num_):
    if reac_ not in elements_.keys():
        reactions_, elements_ = add_element(reactions_, elements_, reac_)
        leftovers[reac_] = 0
    reactions_[list(elements_.keys()).index(prod_)][list(elements_.keys()).index(reac_)] = num_


f = open('input14.txt', mode='r')
for line in f:
    reactant_side, product_side = line.strip().split('=>')
    nop, product = product_side.split()
    if product not in elements.keys():
        reactions, elements = add_element(reactions, elements, product)
        leftovers[product] = 0
    elements[product] = int(nop)
    reactants = reactant_side.split(',')
    for item in reactants:
        nor, reactant = item.split()
        add_reactants(reactions, elements, product, reactant, int(nor))

print(elements)
print(leftovers)
for line in reactions:
    print(line)

ore_count = 0


def how_much_ore_for(no=1, material='FUEL'):
    global ore_count
    global leftovers
    if material == 'ORE':
        ore_count += no
    #  print('Current ore count:', ore_count)
    elif leftovers[material] >= no:
        leftovers[material] -= no
    #  print('Current leftovers:', leftovers)
    else:
        no -= leftovers[material]
        leftovers[material] = 0
        if no // elements[material] == no / elements[material]:
            repetitions = no // elements[material]
        else:
            repetitions = no // elements[material] + 1
            leftovers[material] += repetitions * elements[material] - no
        #  print('Added to leftover: ', repetitions*elements[material]-no)
        mat_row = list(elements.keys()).index(material)
        for index in range(len(elements)):
            if reactions[mat_row][index]:
                print(reactions[mat_row][index] * repetitions, list(elements.keys())[index])
                how_much_ore_for(reactions[mat_row][index] * repetitions, list(elements.keys())[index])


how_much_ore_for(1, 'FUEL')
print(ore_count, 'ORE needed.')
print(leftovers)
