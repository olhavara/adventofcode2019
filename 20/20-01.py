from collections import defaultdict as dd

Map = []
with open('input20.txt', 'r') as f:
    while True:
        line = f.readline()
        if len(line) > 0:
            Map.append(line[:-1])
        if not line:
            break

width = 0
for line in Map:
    if width<len(line):
        width = len(line)
height = len(Map)
# print('width',width,'height',height)


for _ in range(height):
    if len(Map[_])<width:
        Map[_] = Map[_] + ' '*(width-len(Map[_]))


def print_map():
    for line in Map:
        print(line)
print_map()


def find_portals():
    _portals = dict()
    for _row in range(1,height-1):
        for _col in range(1,width-1):
            if Map[_row][_col].isupper():
                found = False
                if Map[_row-1][_col].isupper() and Map[_row+1][_col] == '.':
                    new_key=tuple(([Map[_row-1][_col], Map[_row][_col]]))
                    new_value = {(_row+1, _col)}
                    found = True
                elif Map[_row-1][_col] == '.' and Map[_row+1][_col].isupper():
                    new_key=tuple(([Map[_row][_col], Map[_row+1][_col]]))
                    new_value = {(_row-1, _col)}
                    found = True
                elif Map[_row][_col-1] == '.' and Map[_row][_col+1].isupper():
                    new_key=tuple(([Map[_row][_col], Map[_row][_col+1]]))
                    new_value = {(_row, _col-1)}
                    found = True
                elif Map[_row][_col-1].isupper() and Map[_row][_col+1] == '.':
                    new_key=tuple(([Map[_row][_col-1], Map[_row][_col]]))
                    new_value = {(_row, _col+1)}
                    found = True
                if found:
                    if new_key in _portals.keys():
                        _portals[new_key] = _portals[new_key].union(new_value)
                    else:
                        _portals[new_key] = new_value
    return _portals


portals = find_portals()
# print('portals',len(portals),  portals)
start = portals[('A', 'A')]
end = portals[('Z', 'Z')]
# print('start',start, 'end', end)



def find_vertices():
    _vertices = set()
    for row in range(1, height - 1):
        for col in range(1, width - 1):
            if Map[row][col] == '.':
                wall = 0
                if Map[row - 1][col] == '#':
                    wall += 1
                if Map[row + 1][col] == '#':
                    wall += 1
                if Map[row][col - 1] == '#':
                    wall += 1
                if Map[row][col + 1] == '#':
                    wall += 1
                if wall != 2:
                    _vertices.add((row, col))
    return _vertices

set_of_portals = set()
for portal in portals.values():
    set_of_portals = set_of_portals.union(portal)
# print('set_of_portals', len(set_of_portals), )


set_of_vertices = find_vertices().union(set_of_portals)
# print('set_of_vertices', len(set_of_vertices), set_of_vertices)
list_of_vertices = list(set_of_vertices)
# print('list_of_vertices', len(list_of_vertices), list_of_vertices)


def find_neighbours():
    def go_direction(was_x, was_y, is_x, is_y):
        _length = 1
        _gates = []
        while (is_x, is_y) not in set_of_vertices:
            # print((was_x, was_y), Map[was_x][was_y])
            # print((is_x, is_y), Map[is_x][is_y])
            # input('Press anything...')
            if ((is_x + 1, is_y) != (was_x, was_y)) and Map[is_x + 1][is_y] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x + 1, is_y)
                _length += 1
            elif ((is_x - 1, is_y) != (was_x, was_y)) and Map[is_x - 1][is_y] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x - 1, is_y)
                _length += 1
            elif ((is_x, is_y - 1) != (was_x, was_y)) and Map[is_x][is_y - 1] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x, is_y - 1)
                _length += 1
            elif ((is_x, is_y + 1) != (was_x, was_y)) and Map[is_x][is_y + 1] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x, is_y + 1)
                _length += 1
        # print('found route to ', is_x, is_y, 'with length ', _length)
        # input('Press anything...')
        return is_x, is_y, [_length]

    for index_1 in range(len(list_of_vertices)):
        # print(index_1)
        if index_1 == 0:
            _adjecants = [[]]
        else:
            _adjecants.append([])
        _row, _col = list_of_vertices[index_1]
        for r, c in {(_row - 1, _col), (_row + 1, _col), (_row, _col - 1), (_row, _col + 1)}:
            if Map[r][c] != '#' and not Map[r][c].isupper():
                # print('start at', _row, _col)
                n_x, n_y, lent = go_direction(_row, _col, r, c)
                index_2 = list_of_vertices.index((n_x, n_y))
                _adjecants[index_1].append([index_2] + lent)
    return _adjecants

adjecants = find_neighbours()

# print('adjecants', len(adjecants), adjecants)


del portals[('A', 'A')]
del portals[('Z', 'Z')]
# print('portals', len(portals), portals)

def add_portal_edges():
    for portal_pair in portals.values():
        listp = list(portal_pair)
        index_1, index_2 = list_of_vertices.index(listp[0]),list_of_vertices.index(listp[1])
        adjecants[index_1].append([index_2,1])
        adjecants[index_2].append([index_1,1])

add_portal_edges()


def find_shortest_path():
    start_index = list_of_vertices.index(start)
    end_index = list_of_vertices.index(end)
    nonvisited = dict()
    for _ in range(len(list_of_vertices)):
        nonvisited[_] = 999999
    # print('start',start)
    nonvisited[start_index] = 0
    visited = dict()
    # print('nonvisited', nonvisited)
    while True:
        position = min(nonvisited, key=nonvisited.get)
        length = nonvisited.pop(position)
        # print(length)
        # input('Press anything .. ')
        # print(position == end_index)
        if position == end_index:
            return length
        else:
            for neighbour in adjecants[position]:
                # print('visited.keys()',visited.keys())
                # print(neighbour)
                # input('sth')
                if (neighbour[0] not in visited.keys()):
                    if length + neighbour[1] < nonvisited[neighbour[0]]:
                        nonvisited[neighbour[0]] = length + neighbour[1]
            visited[position] = length

start = start.pop()
end = end.pop()
# print('start',start, 'end', end)

print('Shortest path from AA to ZZ measures:', find_shortest_path())
