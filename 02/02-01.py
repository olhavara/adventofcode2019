INPUT = [1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 1, 6, 19, 1, 9, 19,
         23, 2, 23, 10, 27, 1, 27, 5, 31, 1, 31, 6, 35, 1, 6, 35, 39, 2, 39,
         13, 43, 1, 9, 43, 47, 2, 9, 47, 51, 1, 51, 6, 55, 2, 55, 10, 59, 1,
         59, 5, 63, 2, 10, 63, 67, 2, 9, 67, 71, 1, 71, 5, 75, 2, 10, 75, 79,
         1, 79, 6, 83, 2, 10, 83, 87, 1, 5, 87, 91, 2, 9, 91, 95, 1, 95, 5, 99,
         1, 99, 2, 103, 1, 103, 13, 0, 99, 2, 14, 0, 0]

INPUT[1] = 12
INPUT[2] = 2


def Add(k):
    INPUT[INPUT[k + 3]] = INPUT[INPUT[k + 1]] + INPUT[INPUT[k + 2]]


def Multiply(j):
    INPUT[INPUT[j + 3]] = INPUT[INPUT[j + 1]] * INPUT[INPUT[j + 2]]


i = 0
while i < len(INPUT) and INPUT[i] != 99:
    if INPUT[i] == 1:
        Add(i)
    elif INPUT[i] == 2:
        Multiply(i)
    i += 4

print(INPUT)
