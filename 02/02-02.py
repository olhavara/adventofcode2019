INPUT_orig = [1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 1, 6, 19, 1, 9, 19,
              23, 2, 23, 10, 27, 1, 27, 5, 31, 1, 31, 6, 35, 1, 6, 35, 39, 2, 39,
              13, 43, 1, 9, 43, 47, 2, 9, 47, 51, 1, 51, 6, 55, 2, 55, 10, 59, 1,
              59, 5, 63, 2, 10, 63, 67, 2, 9, 67, 71, 1, 71, 5, 75, 2, 10, 75, 79,
              1, 79, 6, 83, 2, 10, 83, 87, 1, 5, 87, 91, 2, 9, 91, 95, 1, 95, 5, 99,
              1, 99, 2, 103, 1, 103, 13, 0, 99, 2, 14, 0, 0]

RESULT = 19690720
INPUT = INPUT_orig


def Add(i, _INPUT):
    global Broken
    if i + 3 < len(_INPUT):
        _INPUT[_INPUT[i + 3]] = _INPUT[_INPUT[i + 1]] + _INPUT[_INPUT[i + 2]]
    else:
        Broken = True


def Multiply(i, _INPUT):
    global Broken
    if i + 3 < len(_INPUT):
        _INPUT[_INPUT[i + 3]] = _INPUT[_INPUT[i + 1]] * _INPUT[_INPUT[i + 2]]
    else:
        Broken = True


def return_value(Noun, Verb, INPUT_):
    INPUT_[1] = Noun
    INPUT_[2] = Verb
    i = 0
    Broken = False
    while i < len(INPUT_) and INPUT_[i] != 99 and not Broken:
        if INPUT_[i] == 1:
            Add(i, INPUT_)
        elif INPUT_[i] == 2:
            Multiply(i, INPUT_)
        i += 4
    if not Broken:
        return INPUT_[0]
    else:
        return -1


Done = False
Noun = 0
while not Done and Noun < 100:
    Verb = 0
    while not Done and Verb < 100:
        INPUT = INPUT_orig.copy()
        if return_value(Noun, Verb, INPUT) == RESULT:
            Done = True
            print('Noun is ', Noun)
            print('Verb is ', Verb)
        Verb += 1
    Noun += 1
