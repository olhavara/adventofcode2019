import logging
from collections import defaultdict

logging.basicConfig(filename='example.log', level=logging.DEBUG)
open('example.log', 'w').close()

Image_list = []
with open('input08.txt', 'r') as f:
    while True:
        c = f.read(1)
        if not c:
            break
        Image_list.append(int(c))

WIDTH = 25
HEIGHT = 6
LAYER = len(Image_list) // (WIDTH * HEIGHT) if len(Image_list) % (WIDTH * HEIGHT) == 0 else len(Image_list) // (
            WIDTH * HEIGHT)

Image_data = []
for layer in range(LAYER):
    Image_data.append([])

for layer in range(LAYER):
    for height in range(HEIGHT):
        Image_data[layer].append(defaultdict(list))

for layer in range(LAYER):
    for height in range(HEIGHT):
        for width in range(WIDTH):
            Image_data[layer][height][width] = Image_list.pop(0)


def which_layer_min(number):
    min_count = HEIGHT * WIDTH + 1
    min_layer = -1
    for layer in range(LAYER):
        count = 0
        for height in range(HEIGHT):
            for width in range(WIDTH):
                if Image_data[layer][height][width] == number:
                    count += 1
        if min_count > count:
            min_count = count
            min_layer = layer
    return min_layer


def count_on_layer(layer, number):
    count = 0
    for height in range(HEIGHT):
        for width in range(WIDTH):
            if Image_data[layer][height][width] == number:
                count += 1
    return count


def render(data):
    Image = []
    for height in range(HEIGHT):
        Image.append(list())
    for height in range(HEIGHT):
        for width in range(WIDTH):
            layer = 0
            while data[layer][height][width] == 2:
                layer += 1
            Image[height].append(data[layer][height][width])
    print(Image[0])
    print(Image[1])
    print(Image[2])
    print(Image[3])
    print(Image[4])
    print(Image[5])


render(Image_data)
