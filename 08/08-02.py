import logging
from collections import defaultdict

logging.basicConfig(filename='example.log', level=logging.DEBUG)
open('example.log', 'w').close()

Image_list = []
with open('input08.txt', 'r') as f:
    while True:
        c = f.read(1)
        if not c:
            break
        Image_list.append(int(c))

WIDTH = 25
HEIGHT = 6
LAYER = len(Image_list) // (WIDTH * HEIGHT) if len(Image_list) % (WIDTH * HEIGHT) == 0 else len(Image_list) // (
            WIDTH * HEIGHT)

Image = []
for layer in range(LAYER):
    Image.append([])

for layer in range(LAYER):
    for height in range(HEIGHT):
        Image[layer].append(defaultdict(list))

for layer in range(LAYER):
    for height in range(HEIGHT):
        for width in range(WIDTH):
            Image[layer][height][width] = Image_list.pop(0)


def which_layer_min(number):
    min_count = HEIGHT * WIDTH + 1
    min_layer = -1
    for layer in range(LAYER):
        count = 0
        for height in range(HEIGHT):
            for width in range(WIDTH):
                if Image[layer][height][width] == number:
                    count += 1
        if min_count > count:
            min_count = count
            min_layer = layer
    return min_layer


def count_on_layer(layer, number):
    count = 0
    for height in range(HEIGHT):
        for width in range(WIDTH):
            if Image[layer][height][width] == number:
                count += 1
    return count


Min_lay = which_layer_min(0)
Output = count_on_layer(Min_lay, 1) * count_on_layer(Min_lay, 2)
print(Output)
