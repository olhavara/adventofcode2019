# For check tag line 90. There should be no change.

instructions = []
with open('input22.txt', 'r') as f:
    while True:
        line = f.readline()
        if len(line) > 0:
            instructions.append(line[:-1])
        if not line:
            break

# print(instructions)

deck_volume = 119315717514047
shuffles = 101741582076661
position = 93750418158025


def deal_into_new_stack(_position, _deck_volume):
    return (_deck_volume - 1), (_deck_volume - 1)


def cut_card(_n, _position, _deck_volume):
    return 1, deck_volume - _n


def deal_with_increment(_n, _position, _deck_volume):
    return _n, 0


def egcd(c, d):
    if c == 0:
        return d, 0, 1
    else:
        g, y, x = egcd(d % c, c)
        return g, x - (d // c) * y, y


def modinv(to_be_inverted, m):
    g, x, y = egcd(to_be_inverted, m)
    if g != 1:
        raise Exception('modular inverse does not exist')
    else:
        return x % m


a = 1
b = 0
next_a, next_b = None, None
while instructions:
    new_instr = instructions.pop(0)
    if new_instr.startswith('cut '):
        next_a, next_b = cut_card(int(new_instr[4:]), position, deck_volume)
    elif new_instr.startswith('deal with increment '):
        next_a, next_b = deal_with_increment(int(new_instr[len('deal with increment '):]), position, deck_volume)
    elif new_instr == 'deal into new stack':
        next_a, next_b = deal_into_new_stack(position, deck_volume)
    else:
        print('Something wrong.')
    # print(next_a, next_b)
    b = (b * next_a + next_b) % deck_volume
    a = (a * next_a) % deck_volume
    # print(a, b)
    # input()

print('---------------------- one shuffle ----------------------')
print('position', position)
print('a', a, 'b', b)
new_position = (a * position + b) % deck_volume
print('new_position', new_position)

inverse_a = modinv(a, deck_volume)
inverse_b = ((-b) * inverse_a) % deck_volume
print('inverse_a', inverse_a, 'inverse_b', inverse_b)

old_position = (inverse_a * new_position + inverse_b) % deck_volume
print('old_position', old_position)
if old_position == position:
    print('--- OK ---')
else:
    print('Something wrong!')

print('---------------------- many shuffle ----------------------')
print('position', position)
a_many_shuffles = pow(a, shuffles, deck_volume)
b_many_shuffles = ((a_many_shuffles - 1) * modinv(a - 1, deck_volume) * b) % deck_volume
print('a_many_shuffles', a_many_shuffles, 'b_many_shuffles', b_many_shuffles)

new_position_shuffles = (a_many_shuffles * position + b_many_shuffles) % deck_volume
# new_position_shuffles = 2020
print('new_position_shuffles', new_position_shuffles)
inverse_a_many_shuffles = modinv(a_many_shuffles, deck_volume)
inverse_b_many_shuffles = ((-b_many_shuffles) * inverse_a_many_shuffles) % deck_volume
print('inverse_a_many_shuffles', inverse_a_many_shuffles, 'inverse_b_many_shuffles', inverse_b_many_shuffles)

old_position_shuffles = (inverse_a_many_shuffles * new_position_shuffles + inverse_b_many_shuffles) % deck_volume
print('old_position_shuffles', old_position_shuffles)

if old_position_shuffles == position:
    print('--- OK also with shuffles---')
else:
    print('Something wrong!')

print('Result is ', old_position_shuffles)
