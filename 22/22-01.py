instructions = []
with open('input22.txt', 'r') as f:
    while True:
        line = f.readline()
        if len(line) > 0:
            instructions.append(line[:-1])
        if not line:
            break

# print(instructions)

deck = []
counter = 0
while len(deck) < 10007:
    deck.append(counter)
    counter += 1


def deal_into_new_stack(_deck):
    return _deck[::-1]


def cut_card(_n, _deck):
    return _deck[_n:] + _deck[:_n]


def deal_with_increment(_n, _deck):
    new_deck = [None]*len(_deck)
    for _ in range(len(_deck)):
        new_deck[(_n*_) % len(_deck)] = _deck[_]
    return new_deck


OUTPUT = []
while instructions:
    new_instr = instructions.pop(0)
    if new_instr.startswith('cut '):
        deck = cut_card(int(new_instr[4:]), deck)
    elif new_instr.startswith('deal with increment '):
        deck = deal_with_increment(int(new_instr[len('deal with increment '):]), deck)
    elif new_instr == 'deal into new stack':
        deck = deal_into_new_stack(deck)
    OUTPUT.append(new_instr)
    index = deck.index(2019)
    OUTPUT.append(index)

# print(len(OUTPUT), OUTPUT)
print(deck.index(2019))



