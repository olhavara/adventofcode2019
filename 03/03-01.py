import collections

f = open('input03.txt', 'r')
line1 = f.readline().strip().split(',')
line2 = f.readline().strip().split(',')
f.close()
grid_dict = collections.defaultdict(int)
crossroads = list()
print(line1, len(line1), line1[-1])


def draw_first_line(lisst):
    pos_x = 0
    pos_y = 0
    for no_of_seg in range(len(lisst)):
        segment = lisst[no_of_seg]
        if segment[0] == 'U':
            count = int(segment[1:])
            while count:
                pos_y += 1
                if not grid_dict.get((pos_x, pos_y)):
                    grid_dict[(pos_x, pos_y)] += 1
                count -= 1
        elif segment[0] == 'D':
            count = int(segment[1:])
            while count:
                pos_y -= 1
                if not grid_dict.get((pos_x, pos_y)):
                    grid_dict[(pos_x, pos_y)] += 1
                count -= 1
        elif segment[0] == 'R':
            count = int(segment[1:])
            while count:
                pos_x += 1
                if not grid_dict.get((pos_x, pos_y)):
                    grid_dict[(pos_x, pos_y)] += 1
                count -= 1
        elif segment[0] == 'L':
            count = int(segment[1:])
            while count:
                pos_x -= 1
                if not grid_dict.get((pos_x, pos_y)):
                    grid_dict[(pos_x, pos_y)] += 1
                count -= 1


def generate_crossroads(liist):
    pos_x = 0
    pos_y = 0
    for no_of_seg in range(len(liist)):
        segment = liist[no_of_seg]
        if segment[0] == 'U':
            count = int(segment[1:])
            while count:
                pos_y += 1
                if grid_dict.get((pos_x, pos_y)):
                    crossroads.append((pos_x, pos_y))
                count -= 1
        elif segment[0] == 'D':
            count = int(segment[1:])
            while count:
                pos_y -= 1
                if grid_dict.get((pos_x, pos_y)):
                    crossroads.append((pos_x, pos_y))
                count -= 1
        elif segment[0] == 'R':
            count = int(segment[1:])
            while count:
                pos_x += 1
                if grid_dict.get((pos_x, pos_y)):
                    crossroads.append((pos_x, pos_y))
                count -= 1
        elif segment[0] == 'L':
            count = int(segment[1:])
            while count:
                pos_x -= 1
                if grid_dict.get((pos_x, pos_y)):
                    crossroads.append((pos_x, pos_y))
                count -= 1


def find_min():
    crossroad = crossroads.pop()
    min_dist = abs(crossroad[0]) + abs(crossroad[1])
    while crossroads:
        crossroad = crossroads.pop()
        candidate = abs(crossroad[0]) + abs(crossroad[1])
        if candidate < min_dist:
            min_dist = candidate
    return min_dist


draw_first_line(line1)
generate_crossroads(line2)
print(crossroads)
print(find_min())
