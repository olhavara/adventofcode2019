

masses = []
with open('01_01_input.txt') as inputfile:
    for line in inputfile:
        masses.append(int(line.strip()))

fuels=[]
for mass in masses:
    fuel_actual=[]
    Done=False
    actual=mass
    while not Done:
        actual=actual//3-2
        if actual>0:
            fuel_actual.append(actual)
        else:
            Done=True
    fuels.append(sum(fuel_actual))

print(masses)
print(fuels)
print(sum(fuels))
    
