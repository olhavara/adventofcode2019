

masses = []
with open('01_01_input.txt') as inputfile:
    for line in inputfile:
        masses.append(int(line.strip()))

fuels=[]
for mass in masses:
    fuels.append(mass//3-2)

print(masses)
print(fuels)
print(sum(fuels))
    
