DIGITS = []
for i in range(1, 10):
    DIGITS.append(str(i))

DICT = dict()
for i in DIGITS:
    DICT[i] = False

Min = 284639
Max = 748759


def iterate(dictio):
    dict_temp = dict()
    for String in dictio:
        for digit in range(int(String[-1]), 10):
            dict_temp[String + str(digit)] = dictio[String] or (String[-1] == str(digit))
    return dict_temp


for _ in range(5):
    DICT = iterate(DICT)


def check_cond(strin):
    for I in range(1, len(strin) - 2):
        if strin[I] == strin[I + 1] and strin[I] != strin[I - 1] and strin[I + 1] != strin[I + 2]:
            return True
    if strin[0] == strin[1] and strin[2] != strin[1]:
        return True
    elif strin[-1] == strin[-2] and strin[-2] != strin[-3]:
        return True
    else:
        return False


count = 0
for key in DICT:
    if DICT[key] and (Min <= int(key) <= Max) and check_cond(key):
        count += 1

print(count)
