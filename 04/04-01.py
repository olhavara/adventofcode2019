DIGITS = []
for i in range(1, 10):
    DIGITS.append(str(i))

DICT = dict()
for i in DIGITS:
    DICT[i] = False

Min = 284639
Max = 748759


def iterate(dictio):
    dict_temp = dict()
    for String in dictio:
        for digit in range(int(String[-1]), 10):
            dict_temp[String + str(digit)] = dictio[String] or (String[-1] == str(digit))
    return dict_temp


for _ in range(5):
    DICT = iterate(DICT)

count = 0
for key in DICT:
    if DICT[key] and (Min <= int(key) <= Max):
        count += 1

print(count)
