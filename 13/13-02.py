import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
from copy import deepcopy as dc
from collections import defaultdict

open('example.log', 'w').close()
f = open('input13-adjusted.txt', 'r')
Diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1, 99: 0})
chars = dict({0: ' ', 1: '#', 2: 'B', 3: '-', 4: 'O'})
screen = []
score = 0
x_trace_of_ball = 20
x_trace_of_stick = 22

diag_prog = defaultdict(int)
for _ in range(len(Diag_prog)):
    diag_prog[_] = Diag_prog[_]

SuperDone = False
Relative_base = 0
Done = False
OUTPUT = []
OUTPUT_pointer = 0


def Test(i):
    global SuperDone
    global Done
    global diag_prog
    global pointer
    global Relative_base
    instruction = str(diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]
    logging.debug(' opcode: %s, modes: %s', op_code, modes)

    def Mode(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            return diag_prog[diag_prog[_pos + param + 1]]
        elif int(modes[param]) == 1:  # immediate mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[diag_prog[_pos + param + 1] + Relative_base]

    def ModeInput(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 1:  # immediate mode
            return _pos + param + 1
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[_pos + param + 1] + Relative_base

    def Add(_modes, pos):
        logging.debug(' Adding:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)
        # diag_prog[diag_prog[pos+3]]=Mode(_modes,pos,0) + Mode(_modes,pos,1)

    def Multiply(_modes, pos):
        logging.debug(' Multiplying:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)
        # diag_prog[diag_prog[pos+3]]=Mode(_modes,pos,0) * Mode(_modes,pos,1)

    def Jump_if_true(_modes, pos):
        if Mode(_modes, pos, 0):
            logging.debug('%s is not zero, so jumping to  %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
            return Mode(_modes, pos, 1)
        logging.debug('%s is not zero, so not jumping', Mode(_modes, pos, 0))
        return None

    def Jump_if_false(_modes, pos):
        if not Mode(_modes, pos, 0):
            logging.debug('%s is zero, so jumping to  %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
            return Mode(_modes, pos, 1)
        logging.debug('%s is not zero, so not jumping', Mode(_modes, pos, 0))
        return None

    def Read(_modes, pos):
        global Relative_base
        global OUTPUT_pointer
        global x_trace_of_ball
        global x_trace_of_stick
        if not screen:
            _width = max(OUTPUT[::3]) + 1
            _height = max(OUTPUT[1::3]) + 1
            OUTPUT_pointer = load_screen(_width, _height)
            print_screen()
            print(x_trace_of_ball, x_trace_of_stick, len(OUTPUT), OUTPUT_pointer, OUTPUT)
        else:
            OUTPUT_pointer = adjust_screen(OUTPUT_pointer)
            print(x_trace_of_ball, x_trace_of_stick, len(OUTPUT), OUTPUT_pointer, OUTPUT)
        if x_trace_of_stick - x_trace_of_ball == 1:
            INPUT = -1
        elif x_trace_of_stick - x_trace_of_ball == -1:
            INPUT = 1
        else:
            INPUT = 0
        if not int(_modes[0]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            diag_prog[diag_prog[pos + 1]] = INPUT
        elif int(_modes[0]) == 2:  # relative mode
            diag_prog[diag_prog[pos + 1] + Relative_base] = INPUT

    def Write(_modes, pos):
        global SuperDone
        if not int(_modes[0]):  # position mode
            if diag_prog[pos + 1] not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[diag_prog[pos + 1]])
        elif int(_modes[0]) == 1:  # immediate mode
            if pos + 1 not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[pos + 1])
        elif int(_modes[0]) == 2:  # relative mode
            if diag_prog[pos + 1] + Relative_base not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[diag_prog[pos + 1] + Relative_base])

    def Is_less_than(_modes, pos):
        logging.debug(' Comparing:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))
        logging.debug(' P(%s is less than %s), to position %s', Mode(_modes, pos, 0),
                      Mode(_modes, pos, 1),
                      diag_prog[pos + 3])

    def Is_equal(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))
        logging.debug(' P(%s equals %s) to position %s', Mode(_modes, pos, 0),
                      Mode(_modes, pos, 1),
                      pos + 3)

    def Adjust_relative_base(_modes, pos):
        global Relative_base
        Relative_base += Mode(_modes, pos, 0)
        logging.debug(' Adjusting relative base by %s to %s', Mode(_modes, pos, 0), Relative_base)

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        9: Adjust_relative_base,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 4) or (7 <= op_code <= 9):
        Opcode_dict[op_code](modes, i)
        return (number_of_par[op_code] + 1)
    else:
        Jump = Opcode_dict[op_code](modes, i)
        if not Jump:
            return (number_of_par[op_code] + 1)
        else:
            return Jump - pointer


def load_screen(width, height):
    bod = 2
    for y in range(height):
        line = ''
        for x in range(width):
            line += chars[OUTPUT[bod]]
            bod += 3
        screen.append(list(line))
    return len(OUTPUT)


def adjust_screen(OP):
    global width
    global height
    global score
    global x_trace_of_ball
    global x_trace_of_stick
    for _ in range((len(OUTPUT) - OP) // 3):
        x, y, type = OUTPUT[OP], OUTPUT[OP + 1], OUTPUT[OP + 2]
        if x == -1:
            score = dc(type)
        else:
            screen[y][x] = chars[type]
            if type == 4:
                x_trace_of_ball = x
            elif type == 3:
                x_trace_of_stick = x
        OP += 3
    print_screen()
    return OP


def print_screen():
    global score
    for line in screen:
        print("".join(line))
    print('Current score is: ', score)


Done = False
pointer = 0
while not Done:
    posun = Test(pointer)
    pointer += posun

OUTPUT_pointer = adjust_screen(OUTPUT_pointer)
print_screen()
