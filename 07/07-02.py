import logging
from itertools import permutations
from copy import deepcopy

logging.basicConfig(filename='example.log', level=logging.DEBUG)

open('example.log', 'w').close()
f = open('input07.txt', 'r')
_diag_prog = list(map(int, f.readline().strip().split(',')))
diag_prog = [deepcopy(_diag_prog), deepcopy(_diag_prog), deepcopy(_diag_prog), deepcopy(_diag_prog),
             deepcopy(_diag_prog)]
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 99: 0})

Done = False


def Test(i, Amp, INPUT):
    global Done
    global diag_prog
    global pointer
    instruction = str(diag_prog[Amp][i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]
    logging.debug(' opcode: %s, modes: %s', op_code, modes)

    def Mode(modes, _pos, param):
        if not int(modes[param]):
            return diag_prog[Amp][diag_prog[Amp][_pos + param + 1]]
        else:
            return diag_prog[Amp][_pos + param + 1]

    def Add(_modes, pos, Amp):
        logging.debug(' Adding:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[Amp][diag_prog[Amp][pos + 3]] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)

    def Multiply(_modes, pos, Amp):
        logging.debug(' Multiplying:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[Amp][diag_prog[Amp][pos + 3]] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)

    def Jump_if_true(_modes, pos):
        global Amp
        if Mode(_modes, pos, 0):
            logging.debug('%s is not zero, so jumping to  %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
            return Mode(_modes, pos, 1)
        logging.debug('%s is not zero, so not jumping', Mode(_modes, pos, 0))
        return None

    def Jump_if_false(_modes, pos):
        global Amp
        if not Mode(_modes, pos, 0):
            logging.debug('%s is zero, so jumping to  %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
            return Mode(_modes, pos, 1)
        logging.debug('%s is not zero, so not jumping', Mode(_modes, pos, 0))
        return None

    def Read(_modes, pos, Amp, INPUT):
        global Incident
        if not INPUT[Amp]:
            Incident = True
        else:
            diag_prog[Amp][diag_prog[Amp][pos + 1]] = INPUT[Amp].pop(0)

    def Write(_modes, pos, Amp, INPUT):
        INPUT[(Amp + 1) % 5].append(Mode(_modes, pos, 0))

    def Is_less_than(_modes, pos, Amp):
        logging.debug(' Adding:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        if not int(modes[2]):
            diag_prog[Amp][diag_prog[Amp][pos + 3]] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))
            logging.debug(' P(%s is less than %s), to position %s', Mode(_modes, pos, 0),
                          Mode(_modes, pos, 1),
                          diag_prog[Amp][pos + 3])
        else:
            diag_prog[Amp][_pos + 3] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))

    def Is_equal(_modes, pos, Amp):
        if not int(modes[2]):
            diag_prog[Amp][diag_prog[Amp][pos + 3]] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))
            logging.debug(' P(%s equals %s) to position %s', Mode(_modes, pos, 0),
                          Mode(_modes, pos, 1),
                          pos + 3)
        else:
            diag_prog[Amp][_pos + 3] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 2) or (7 <= op_code <= 8):
        Opcode_dict[op_code](modes, i, Amp)
        return number_of_par[op_code] + 1
    elif 3 <= op_code <= 4:
        Opcode_dict[op_code](modes, i, Amp, INPUT)
        return (number_of_par[op_code] + 1) if not Incident else 0
    else:
        Jump = Opcode_dict[op_code](modes, i)
        if not Jump:
            return number_of_par[op_code] + 1
        else:
            return Jump - pointer[Amp]


Max_thrust = 0
Input = []
for i in permutations('56789', 5):
    INPUT = [[int(i[0]), 0], [int(i[1])], [int(i[2])], [int(i[3])], [int(i[4])]]  # 0=AmpA, 1=AmpB, ... , 4=AmpE
    print(INPUT)
    for sth in range(5):
        Done = False
        if sth == 0:
            pointer = [0, 0, 0, 0, 0]
            Amplifier = 0
        while not Done:
            Incident = False
            while not Incident and not Done:
                posun = Test(pointer[Amplifier], Amplifier, INPUT)
                pointer[Amplifier] += posun
                logging.debug('%s', Amplifier)
            Amplifier = (Amplifier + 1) % 5
    if Max_thrust < INPUT[0][0]:
        Max_thrust = INPUT[0][0]
    diag_prog = [deepcopy(_diag_prog), deepcopy(_diag_prog), deepcopy(_diag_prog), deepcopy(_diag_prog),
                 deepcopy(_diag_prog)]

print(Max_thrust)
