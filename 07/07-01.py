import logging
from itertools import permutations

logging.basicConfig(filename='example.log', level=logging.DEBUG)

open('example.log', 'w').close()
f = open('input07.txt', 'r')
diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 99: 0})

Done = False
OUTPUT = []


def Test(i):
    global Done
    global diag_prog
    global pointer
    instruction = str(diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]

    def Mode(modes, _pos, param):
        if not int(modes[param]):
            return diag_prog[diag_prog[_pos + param + 1]]
        else:
            return diag_prog[_pos + param + 1]

    def Add(_modes, pos):
        diag_prog[diag_prog[pos + 3]] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)

    def Multiply(_modes, pos):
        diag_prog[diag_prog[pos + 3]] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)

    def Jump_if_true(_modes, pos):
        if Mode(_modes, pos, 0):
            return Mode(_modes, pos, 1)
        return None

    def Jump_if_false(_modes, pos):
        if not Mode(_modes, pos, 0):
            return Mode(_modes, pos, 1)
        return None

    def Read(_modes, pos):
        diag_prog[diag_prog[pos + 1]] = INPUT.pop(-1)

    def Write(_modes, pos):
        OUTPUT.append(Mode(_modes, pos, 0))

    def Is_less_than(_modes, pos):
        if not int(modes[2]):
            diag_prog[diag_prog[pos + 3]] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))
        else:
            diag_prog[_pos + 3] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))

    def Is_equal(_modes, pos):
        if not int(modes[2]):
            diag_prog[diag_prog[pos + 3]] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))
        else:
            diag_prog[_pos + 3] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 4) or (7 <= op_code <= 8):
        Opcode_dict[op_code](modes, i)
        return (number_of_par[op_code] + 1)
    else:
        Jump = Opcode_dict[op_code](modes, i)
        if not Jump:
            return (number_of_par[op_code] + 1)
        else:
            return Jump - pointer


Max_thrust = 0
for perm in permutations('01234', 5):
    INPUT = [0]
    for phase in perm:
        INPUT.append(int(phase))
        Done = False
        OUTPUT = []
        pointer = 0
        while not Done:
            posun = Test(pointer)
            pointer += posun
        INPUT.append(OUTPUT[-1])
    if OUTPUT[-1] > Max_thrust:
        Max_thrust = OUTPUT[-1]

print(Max_thrust)
