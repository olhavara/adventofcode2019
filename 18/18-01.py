from collections import defaultdict as dd

Map = []
with open('input18.txt', 'r') as f:
    while True:
        line = f.readline().strip()
        if len(line) > 0:
            Map.append(line)
        if not line:
            break
width = len(Map[0])
height = len(Map)


def find_robot():
    for _row in range(height):
        if Map[_row].find('@') > -1:
            return Map[_row].find('@'), _row


def print_map():
    for line in Map:
        print(line)


def find_keys():
    _keys = dict()
    for row in range(height):
        for col in range(width):
            if Map[row][col].islower():
                _keys[Map[row][col]] = (row, col)
    return _keys


def find_vertices():
    _vertices = set()
    for row in range(1, height - 1):
        for col in range(1, width - 1):
            if Map[row][col] != '#':
                wall = 0
                if Map[row - 1][col] == '#':
                    wall += 1
                if Map[row + 1][col] == '#':
                    wall += 1
                if Map[row][col - 1] == '#':
                    wall += 1
                if Map[row][col + 1] == '#':
                    wall += 1
                if wall != 2:
                    _vertices.add((row, col))
    return _vertices


def find_neighbours():
    def go_direction(was_x, was_y, is_x, is_y):
        _length = 1
        _gates = []
        while (is_x, is_y) not in set_of_vertices:
            if Map[is_x][is_y].isupper():
                _gates.append(Map[is_x][is_y])
            if ((is_x + 1, is_y) != (was_x, was_y)) and Map[is_x + 1][is_y] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x + 1, is_y)
                _length += 1
            elif ((is_x - 1, is_y) != (was_x, was_y)) and Map[is_x - 1][is_y] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x - 1, is_y)
                _length += 1
            elif ((is_x, is_y - 1) != (was_x, was_y)) and Map[is_x][is_y - 1] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x, is_y - 1)
                _length += 1
            elif ((is_x, is_y + 1) != (was_x, was_y)) and Map[is_x][is_y + 1] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x, is_y + 1)
                _length += 1
        return is_x, is_y, [_length, _gates]

    for index_1 in range(len(list_of_vertices)):
        if index_1 == 0:
            _adjecants = [[]]
        else:
            _adjecants.append([])
        _row, _col = list_of_vertices[index_1]
        for r, c in {(_row - 1, _col), (_row + 1, _col), (_row, _col - 1), (_row, _col + 1)}:
            if Map[r][c] != '#':
                n_x, n_y, lst = go_direction(_row, _col, r, c)
                index_2 = list_of_vertices.index((n_x, n_y))
                _adjecants[index_1].append([index_2] + lst)
    return _adjecants


keys = find_keys()
set_of_vertices = find_vertices().union(set(keys.values()))
list_of_vertices = list(set_of_vertices)
index_of_keys = set()
for index in range(len(list_of_vertices)):
    if list_of_vertices[index] in keys.values():
        index_of_keys.add(index)
start = list_of_vertices.index(find_robot())
print('start', start)
adjecants = find_neighbours()
print('adjecants',adjecants)
print('list_of_vertices',list_of_vertices)

for k, v in keys.items():
    keys[k] = list_of_vertices.index(v)


def find_shortest_path_to_keys(_adjecant_keys, pos):
    nonvisited = dict()
    for _ in range(len(list_of_vertices)):
        nonvisited[_] = [999999, set()]
    nonvisited[pos] = [0, set()]
    visited = dict()
    min_length = 999999
    position = None
    for p, l in nonvisited.items():
        if l[0] < min_length:
            min_length = l[0]
            position = p
    while min_length < 999999:
        couple = nonvisited[position]
        del nonvisited[position]
        set_of_gates = couple[1]
        if (position not in index_of_keys) or (position == pos):
            for neighbour in adjecants[position]:
                if neighbour[0] not in visited.keys():
                    if min_length + neighbour[1] < nonvisited[neighbour[0]][0]:
                        nonvisited[neighbour[0]][0] = min_length + neighbour[1]
                        nonvisited[neighbour[0]][1] = set_of_gates.union(set(neighbour[2]))
        visited[position] = [min_length, set_of_gates]
        min_length = 999999
        for p, l in nonvisited.items():
            if l[0] < min_length:
                min_length = l[0]
                position = p
    for _key_position in (set(visited.keys()).intersection(set(index_of_keys))):
        if _key_position != pos:
            _adjecant_keys[pos].append([_key_position, visited[_key_position][0], visited[_key_position][1]])
    return _adjecant_keys


paths = [(set(), 0, start)]
print('paths', paths)

def find_shortest_path(_paths):
    curr_gates, curr_len, pos = _paths.pop(0)
    nonvisited = dict()
    for _ in keys.values():
        nonvisited[_] = 999999
    nonvisited[pos] = 0
    visited = dict()
    while nonvisited:
        if min(nonvisited.values()) < 999999:
            position = min(nonvisited, key=nonvisited.get)
            length = nonvisited.pop(position)
            if position not in index_of_keys.difference({keys[gate.lower()] for gate in curr_gates}):
                for neighbour in adjecant_keys[position]:
                    if (len(set(neighbour[2]) - curr_gates) == 0) and (neighbour[0] not in visited.keys()):
                        if length + neighbour[1] < nonvisited[neighbour[0]]:
                            nonvisited[neighbour[0]] = length + neighbour[1]
            visited[position] = length
        else:
            break
    for key_position in (set(visited.keys()).intersection(set(index_of_keys))).difference(
            {keys[gate.lower()] for gate in curr_gates}):
        key = [key for (key, value) in keys.items() if value == key_position][0]
        _paths.append((set(key.upper()).union(curr_gates), curr_len + visited[key_position], key_position))
    return _paths


adjecants = find_neighbours()

adjecant_keys = dd(list)
adjecant_keys = find_shortest_path_to_keys(adjecant_keys, start)
print(list_of_vertices[309])
print(adjecant_keys)

for key_position in keys.values():
    adjecant_keys = find_shortest_path_to_keys(adjecant_keys, key_position)
    print(adjecant_keys)

current = -1
start_of_heuristic = 1
heuristic = 2
# Works also for heuristic = 3 with start_of_h larger than 6
while len(paths[0][0]) < start_of_heuristic:
    if len(paths[0][0]) > current:
        current = len(paths[0][0])
    paths = find_shortest_path(paths)
    print('paths', paths)

while len(paths[0][0]) < 26:
    if len(paths[0][0]) > current:
        current = len(paths[0][0])
        paths = sorted(paths, key=lambda path: path[1])
        paths = paths[:len(paths) // heuristic]
    paths = find_shortest_path(paths)

paths = sorted(paths, key=lambda path: path[1])
print(paths[0:100])
print('Shortest path may be: ', paths[0][1])
