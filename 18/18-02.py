from collections import defaultdict as dd

Map = []
with open('input18-2.txt', 'r') as f:
    while True:
        line = f.readline().strip()
        if len(line) > 0:
            Map.append(line)
        if not line:
            break
width = len(Map[0]) // 2 + 1
height = len(Map) // 2 + 1


def find_robot():
    _robots_list = []
    for vertical_segment, horizontal_segment in [(0, 0), (0, 1), (1, 0), (1, 1)]:
        for row in range(vertical_segment * 40, vertical_segment * 40 + height):
            for col in range(horizontal_segment * 40, horizontal_segment * 40 + width):
                if Map[row][col] == '@':
                    _robots_list.append((row, col))
    return _robots_list


def find_keys():
    _keys_list = []
    for vertical_segment, horizontal_segment in [(0, 0), (0, 1), (1, 0), (1, 1)]:
        _keys = dict()
        for row in range(vertical_segment * 40, vertical_segment * 40 + height):
            for col in range(horizontal_segment * 40, horizontal_segment * 40 + width):
                if Map[row][col].islower():
                    _keys[Map[row][col]] = (row, col)
        _keys_list.append(_keys)
    return _keys_list


def find_vertices(_robots_list):
    _vertices_list = []
    counter = 0
    for vertical_segment, horizontal_segment in [(0, 0), (0, 1), (1, 0), (1, 1)]:
        _vertices = set()
        for row in range(vertical_segment * 40, vertical_segment * 40 + height):
            for col in range(horizontal_segment * 40, horizontal_segment * 40 + width):
                if Map[row][col] != '#':
                    wall = 0
                    if Map[row - 1][col] == '#':
                        wall += 1
                    if Map[row + 1][col] == '#':
                        wall += 1
                    if Map[row][col - 1] == '#':
                        wall += 1
                    if Map[row][col + 1] == '#':
                        wall += 1
                    if wall != 2:
                        _vertices.add((row, col))
        _vertices.add(_robots_list[counter])
        _vertices_list.append(_vertices)
        counter += 1
    return _vertices_list


def find_neighbours():
    def go_direction(was_x, was_y, is_x, is_y):
        # print(was_x,was_y)
        _length = 1
        _gates = []
        while (is_x, is_y) not in set_of_vertices:
            # print(is_x, is_y)
            if Map[is_x][is_y].isupper():
                _gates.append(Map[is_x][is_y])
            if ((is_x + 1, is_y) != (was_x, was_y)) and Map[is_x + 1][is_y] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x + 1, is_y)
                _length += 1
            elif ((is_x - 1, is_y) != (was_x, was_y)) and Map[is_x - 1][is_y] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x - 1, is_y)
                _length += 1
            elif ((is_x, is_y - 1) != (was_x, was_y)) and Map[is_x][is_y - 1] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x, is_y - 1)
                _length += 1
            elif ((is_x, is_y + 1) != (was_x, was_y)) and Map[is_x][is_y + 1] != '#':
                (was_x, was_y) = (is_x, is_y)
                (is_x, is_y) = (is_x, is_y + 1)
                _length += 1
        # print(is_x, is_y)
        return is_x, is_y, [_length, _gates]

    for index_1 in range(len(list_of_vertices)):
        if index_1 == 0:
            _adjecants = [[]]
        else:
            _adjecants.append([])
        _row, _col = list_of_vertices[index_1]
        for r, c in {(_row - 1, _col), (_row + 1, _col), (_row, _col - 1), (_row, _col + 1)}:
            if Map[r][c] != '#':
                # print(index_1,_row,_col, r,c)
                n_x, n_y, lst = go_direction(_row, _col, r, c)
                # print(index_1,_row,_col, r,c,n_x, n_y, lst )
                index_2 = list_of_vertices.index((n_x, n_y))
                # print(n_x,n_y, index_2)
                # print(index_2)
                _adjecants[index_1].append([index_2] + lst)
                # print(list_of_vertices)
                # print(_adjecants)
                # input("Press Enter to continue...")
    return _adjecants


robots_list = find_robot()
keys_list = find_keys()
keys = {**keys_list[0], **keys_list[1], **keys_list[2], **keys_list[3]}
set_of_vertices_list = find_vertices(robots_list)

for _ in range(4):
    set_of_vertices_list[_] = set_of_vertices_list[_].union(set(keys_list[_].values()))
set_of_vertices = set()
list_of_vertices_list = []
for _ in range(4):
    set_of_vertices = set_of_vertices.union(set_of_vertices_list[_])
    print('set_of_vertices_list (part) ---', len(set_of_vertices_list[_]), set_of_vertices_list[_])
    list_of_vertices_list.append(list(set_of_vertices_list[_]))
list_of_vertices = []
for _ in range(4):
    list_of_vertices += list_of_vertices_list[_]

print('set_of_vertices:', len(set_of_vertices), set_of_vertices)
print('list_of_vertices_list', len(list_of_vertices_list[0]),
      len(list_of_vertices_list[1]),
      len(list_of_vertices_list[2]),
      len(list_of_vertices_list[3]), list_of_vertices_list)

index_of_keys = set()
for index in range(len(list_of_vertices)):
    if list_of_vertices[index] in keys.values():
        index_of_keys.add(index)

start_list = []
for _ in range(4):
    start_list.append(list_of_vertices.index(robots_list[_]))
print('start_list', start_list)
adjecants = find_neighbours()
print('adjecants', adjecants)
for _ in range(4):
    for k, v in keys_list[_].items():
        keys_list[_][k] = list_of_vertices_list[_].index(v)

for k, v in keys.items():
    keys[k] = list_of_vertices.index(v)
print('keys', keys)
print('keys_list', keys_list)


def find_shortest_path_to_keys(_adjecant_keys, pos):
    # print('paths:',_paths)
    # print(curr_gates, curr_len, pos)
    nonvisited = dict()
    for _ in range(len(list_of_vertices)):
        nonvisited[_] = [999999, set()]
    nonvisited[pos] = [0, set()]
    # print('nonvisited', nonvisited)
    visited = dict()
    min_length = 999999
    position = None
    for p, l in nonvisited.items():
        if l[0] < min_length:
            min_length = l[0]
            position = p
    # print(min_length, position)
    while min_length < 999999:
        couple = nonvisited[position]
        # print(nonvisited[position])
        # print('couple',couple)
        del nonvisited[position]
        # print('couple',couple)
        set_of_gates = couple[1]
        # print('set_of_gates',set_of_gates)
        # print('nonvisited', nonvisited)
        # print(position, min_length, set_of_gates)
        if (position not in index_of_keys) or (position == pos):
            # print(position not in index_of_keys)
            # print(position == pos)
            for neighbour in adjecants[position]:
                # print(neighbour, list_of_vertices[neighbour[0]])
                if neighbour[0] not in visited.keys():
                    if min_length + neighbour[1] < nonvisited[neighbour[0]][0]:
                        nonvisited[neighbour[0]][0] = min_length + neighbour[1]
                        # print(list_of_vertices[position],list_of_vertices[neighbour[0]],neighbour)
                        nonvisited[neighbour[0]][1] = set_of_gates.union(set(neighbour[2]))
                        # print(nonvisited[neighbour[0]][1])
                        # print(nonvisited)
            # print(nonvisited)
        visited[position] = [min_length, set_of_gates]
        min_length = 999999
        for p, l in nonvisited.items():
            if l[0] < min_length:
                min_length = l[0]
                position = p
        # print(min_length, position)
    for _key_position in (set(visited.keys()).intersection(set(index_of_keys))):
        if _key_position != pos:
            _adjecant_keys[pos].append([_key_position, visited[_key_position][0], visited[_key_position][1]])
    # input('Press enter to continue ...')
    # print('-----------------------------------------------------------------------------')
    return _adjecant_keys


paths = [(set(), 0, start_list)]
print('paths', paths)

def find_shortest_path(_paths):
    def quadrant_test(position_):
        row,col = list_of_vertices[position_]
        if row<41 and col<41:
            return 0
        elif row<41 and col>40:
            return 1
        elif row>40 and col>40:
            return 3
        elif row>40 and col<41:
            return 2

    # print('paths:',len(_paths),_paths)
    curr_gates, curr_len, pos_list = _paths.pop(0)
    # print(curr_gates, curr_len, pos_list)
    nonvisited = dict()
    for _ in keys.values():
        nonvisited[_] = 999999
    for _ in range(4):
        nonvisited[pos_list[_]] = 0
    # print('nonvisited', nonvisited)
    visited = dict()
    # print('visited', visited)
    # input('Press anything')
    while nonvisited:
        if min(nonvisited.values()) < 999999:
            position = min(nonvisited, key=nonvisited.get)
            length = nonvisited.pop(position)
            # print(position, length)
            if position not in index_of_keys.difference({keys[gate.lower()] for gate in curr_gates}):
                for neighbour in adjecant_keys[position]:
                    if (len(set(neighbour[2]) - curr_gates) == 0) and (neighbour[0] not in visited.keys()):
                        if length + neighbour[1] < nonvisited[neighbour[0]]:
                            nonvisited[neighbour[0]] = length + neighbour[1]
            visited[position] = length
            # print('nonvisited', nonvisited)
            # print('visited', visited)
            # input('Press anything')
        else:
            # print('nonvisited', nonvisited)
            # print('visited', visited)
            # print(list_of_vertices[356],list_of_vertices[294],list_of_vertices[154],list_of_vertices[282])
            # print('set', (set(visited.keys()).intersection(set(index_of_keys))).difference(
            # {keys[gate.lower()] for gate in curr_gates}))
            # input('About to break')
            break
    for key_position in (set(visited.keys()).intersection(set(index_of_keys))).difference(
            {keys[gate.lower()] for gate in curr_gates}):
        # print(key_position)
        key = [key for (key, value) in keys.items() if value == key_position][0]
        # print(key)
        quadrant = quadrant_test(key_position)
        temp_pos_list = []
        for _ in range(4):
            temp_pos_list.append(pos_list[_])
        # print(temp_pos_list)
        temp_pos_list[quadrant] = key_position
        # print(temp_pos_list)
        # input('what')
        _paths.append((set(key.upper()).union(curr_gates), curr_len + visited[key_position], temp_pos_list))
    # print('-----------------------------------------------------------------------------')
    return _paths


print('-----------------------------------')
print('width:', width, ', height:', height)
for _ in range(4):
    print(keys_list[_], len(keys_list[_]))
print('keys:', len(keys), keys)
print('list_of_vertices', len(list_of_vertices), list_of_vertices)
print('index_of_keys', len(index_of_keys), index_of_keys)
print('-----------------------------------')

adjecant_keys = dd(list)
for _ in range(4):
    adjecant_keys = find_shortest_path_to_keys(adjecant_keys, start_list[_])


for key_position in keys.values():
    adjecant_keys = find_shortest_path_to_keys(adjecant_keys, key_position)

print('adjecant_keys', len(adjecant_keys), adjecant_keys)
print('----------------------------------------------------------')



current = -1
start_of_heuristic = 7
heuristic = 3
# Works also for heuristic = 3 with start_of_h larger than 6
while len(paths[0][0]) < start_of_heuristic:
    if len(paths[0][0]) > current:
        current = len(paths[0][0])
        print(current, len(paths))
    paths = find_shortest_path(paths)

print('paths', len(paths), paths)
while len(paths[0][0]) < 26:
    if len(paths[0][0]) > current:
        current = len(paths[0][0])
        print(current, len(paths))
        paths = sorted(paths, key=lambda path: path[1])
        paths = paths[:len(paths) // heuristic]
        print(current, len(paths))
    paths = find_shortest_path(paths)

paths = sorted(paths, key=lambda path: path[1])
print(paths[0:100])
print('Shortest path may be: ', paths[0][1])

# print('keys', keys)
# temp=[]
# laci=[]
# temp2=[]
# for k,v in adjecant_keys.items():
#     for t in v:
#         temp.append(t[0])
#         temp2.append(list_of_vertices[t[0]])
# print('temp',temp)
# for k,v in keys.items():
#     if v in temp:
#         laci.append(k)
# print('temp2', temp2)
# print(laci)
# print
