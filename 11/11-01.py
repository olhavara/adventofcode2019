import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
from collections import defaultdict

open('example.log', 'w').close()
f = open('input11.txt', 'r')
Diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1, 99: 0})

diag_prog = defaultdict(int)
for _ in range(len(Diag_prog)):
    diag_prog[_] = Diag_prog[_]

Relative_base = 0
Done = False

OUTPUT = []


def Test(i):
    global Done
    global diag_prog
    global pointer
    global Relative_base
    instruction = str(diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]
    logging.debug(' opcode: %s, modes: %s', op_code, modes)

    def Mode(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            return diag_prog[diag_prog[_pos + param + 1]]
        elif int(modes[param]) == 1:  # immediate mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[diag_prog[_pos + param + 1] + Relative_base]

    def ModeInput(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 1:  # immediate mode
            return _pos + param + 1
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[_pos + param + 1] + Relative_base

    def Add(_modes, pos):
        logging.debug(' Adding:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)
        # diag_prog[diag_prog[pos+3]]=Mode(_modes,pos,0) + Mode(_modes,pos,1)

    def Multiply(_modes, pos):
        logging.debug(' Multiplying:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)
        # diag_prog[diag_prog[pos+3]]=Mode(_modes,pos,0) * Mode(_modes,pos,1)

    def Jump_if_true(_modes, pos):
        if Mode(_modes, pos, 0):
            logging.debug('%s is not zero, so jumping to  %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
            return Mode(_modes, pos, 1)
        logging.debug('%s is not zero, so not jumping', Mode(_modes, pos, 0))
        return None

    def Jump_if_false(_modes, pos):
        if not Mode(_modes, pos, 0):
            logging.debug('%s is zero, so jumping to  %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
            return Mode(_modes, pos, 1)
        logging.debug('%s is not zero, so not jumping', Mode(_modes, pos, 0))
        return None

    def Read(_modes, pos):
        global Relative_base
        if len(OUTPUT) < 2:
            Input = 0
        else:
            Input = Paint_Move_and_Get(OUTPUT[-2], OUTPUT[-1])
        if not int(_modes[0]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            # print('what is intput', Input)
            diag_prog[diag_prog[pos + 1]] = Input
        elif int(_modes[0]) == 2:  # relative mode
            # print('what is intput', Input)
            diag_prog[diag_prog[pos + 1] + Relative_base] = Input

    def Write(_modes, pos):
        if not int(_modes[0]):  # position mode
            OUTPUT.append(diag_prog[diag_prog[pos + 1]])
        elif int(_modes[0]) == 1:  # immediate mode
            OUTPUT.append(diag_prog[pos + 1])
        elif int(_modes[0]) == 2:  # relative mode
            OUTPUT.append(diag_prog[diag_prog[pos + 1] + Relative_base])

    def Is_less_than(_modes, pos):
        logging.debug(' Comparing:%s a %s', Mode(_modes, pos, 0), Mode(_modes, pos, 1))
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))
        logging.debug(' P(%s is less than %s), to position %s', Mode(_modes, pos, 0),
                      Mode(_modes, pos, 1),
                      diag_prog[pos + 3])

    def Is_equal(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))
        logging.debug(' P(%s equals %s) to position %s', Mode(_modes, pos, 0),
                      Mode(_modes, pos, 1),
                      pos + 3)

    def Adjust_relative_base(_modes, pos):
        global Relative_base
        Relative_base += Mode(_modes, pos, 0)
        logging.debug(' Adjusting relative base by %s to %s', Mode(_modes, pos, 0), Relative_base)

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        9: Adjust_relative_base,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 4) or (7 <= op_code <= 9):
        Opcode_dict[op_code](modes, i)
        return (number_of_par[op_code] + 1)
    else:
        Jump = Opcode_dict[op_code](modes, i)
        if not Jump:
            return (number_of_par[op_code] + 1)
        else:
            return Jump - pointer


Grid = dict()
Robot = [(0, -1), (0, 0)]


def Paint_Move_and_Get(paint, direction):
    global Grid
    global Robot

    Grid[Robot[1]] = paint
    gradient = (Robot[1][0] - Robot[0][0], Robot[1][1] - Robot[0][1])
    if gradient == (0, 1):
        if direction:
            Robot.append((Robot[1][0] + 1, Robot[1][1]))
        else:
            Robot.append((Robot[1][0] - 1, Robot[1][1]))
    elif gradient == (1, 0):
        if direction:
            Robot.append((Robot[1][0] + 0, Robot[1][1] - 1))
        else:
            Robot.append((Robot[1][0] + 0, Robot[1][1] + 1))
    elif gradient == (0, -1):
        if direction:
            Robot.append((Robot[1][0] - 1, Robot[1][1] - 0))
        else:
            Robot.append((Robot[1][0] + 1, Robot[1][1] + 0))
    elif gradient == (-1, 0):
        if direction:
            Robot.append((Robot[1][0] + 0, Robot[1][1] + 1))
        else:
            Robot.append((Robot[1][0] + 0, Robot[1][1] - 1))
    Robot.pop(0)
    if Robot[1] in Grid.keys():
        return Grid[Robot[1]]
    else:
        return 0


Done = False
pointer = 0
while not Done:
    posun = Test(pointer)
    pointer += posun

print(len(Grid))
print(Grid)
