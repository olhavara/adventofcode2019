# Arcade game
# Do not need to go west from hull breach
# Right mass is with: mutex, planetoid, antenna and fuel cell

import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
from collections import defaultdict

open('example.log', 'w').close()
f = open('input25.txt', 'r')
Diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1, 99: 0})

diag_prog = defaultdict(int)
for _ in range(len(Diag_prog)):
    diag_prog[_] = Diag_prog[_]

SuperDone = False
Relative_base = 0
Done = False
OUTPUT = []


def Test(i):
    global SuperDone
    global Done
    global diag_prog
    global pointer
    global Relative_base
    global Read_instructions
    instruction = str(diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]
    logging.debug(' opcode: %s, modes: %s', op_code, modes)

    def Mode(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            return diag_prog[diag_prog[_pos + param + 1]]
        elif int(modes[param]) == 1:  # immediate mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[diag_prog[_pos + param + 1] + Relative_base]

    def ModeInput(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 1:  # immediate mode
            return _pos + param + 1
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[_pos + param + 1] + Relative_base

    def Add(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)

    def Multiply(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)

    def Jump_if_true(_modes, pos):
        if Mode(_modes, pos, 0):
            return Mode(_modes, pos, 1)
        return None

    def Jump_if_false(_modes, pos):
        if not Mode(_modes, pos, 0):
            return Mode(_modes, pos, 1)
        return None

    def Read(_modes, pos):
        global Relative_base
        global Read_instructions
        if not int(_modes[0]):  # position mode
            if not Read_instructions:
                word = input("What is the input:")
                for _ in word:
                    Read_instructions.append(ord(_))
                Read_instructions.append(10)
            print(Read_instructions)
            diag_prog[diag_prog[pos + 1]] = Read_instructions.pop(0)
        elif int(_modes[0]) == 2:  # relative mode
            if not Read_instructions:
                word = input("What is the input:")
                for _ in word:
                    Read_instructions.append(ord(_))
                Read_instructions.append(10)
            diag_prog[diag_prog[pos + 1] + Relative_base] = Read_instructions.pop(0)

    def Write(_modes, pos):
        global SuperDone
        if not int(_modes[0]):  # position mode
            if diag_prog[pos + 1] not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[diag_prog[pos + 1]])
        elif int(_modes[0]) == 1:  # immediate mode
            if pos + 1 not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[pos + 1])
        elif int(_modes[0]) == 2:  # relative mode
            if diag_prog[pos + 1] + Relative_base not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[diag_prog[pos + 1] + Relative_base])

    def Is_less_than(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))

    def Is_equal(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))

    def Adjust_relative_base(_modes, pos):
        global Relative_base
        Relative_base += Mode(_modes, pos, 0)

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        9: Adjust_relative_base,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 4) or (7 <= op_code <= 9):
        Opcode_dict[op_code](modes, i)
        return (number_of_par[op_code] + 1)
    else:
        Jump = Opcode_dict[op_code](modes, i)
        if Jump is None:
            return (number_of_par[op_code] + 1)
        else:
            return Jump - pointer


Done = False
pointer = 0
Read_instructions = []
while not Done:
    posun = Test(pointer)
    pointer += posun
    if OUTPUT:
        if OUTPUT[-1] == 10:
            line = ''
            while OUTPUT:
                line += chr(OUTPUT.pop(0))
            print(line)
