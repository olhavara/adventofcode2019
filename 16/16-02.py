Input = []
with open('input16.txt', 'r') as f:
    while True:
        c = f.read(1)
        if len(c.strip()) > 0:
            Input.append(int(c))
        if not c:
            break
Offset = 5979013
Input = Input * 10000
Input = Input[Offset:]

phases = 100

Output = [0] * phases
while Input:
    pivot = Input.pop(-1)
    for i in range(phases):
        Output[i] = (pivot + Output[i]) % 10
        pivot = Output[i]
    if len(Input) < 8:
        print(Output[-1])

print(Input)
