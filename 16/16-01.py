import numpy as np

Input = []
with open('input16.txt', 'r') as f:
    while True:
        c = f.read(1)
        if len(c.strip()) > 0:
            Input.append(float(c))
        if not c:
            break

Input = np.array(Input)

base_pattern = [0, 1, 0, -1]


def generate_pattern_matrix(base_pattern_, scale):
    matrix = np.zeros(shape=(scale, scale))
    for i in range(1, scale + 1):
        row = [val for val in base_pattern_ for _ in range(i)]
        while len(row) - 1 < scale:
            row += row
        matrix[i - 1] = row[1:scale + 1]
    return matrix


def last_digit(x):
    return int(str(int(x))[-1])


pattern_matrix = generate_pattern_matrix(base_pattern, len(Input))
for _ in range(100):
    Input = np.array([last_digit(xi) for xi in np.dot(pattern_matrix, Input)])

print(Input, len(Input))
