import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
from collections import defaultdict

open('example.log', 'w').close()
f = open('input15.txt', 'r')
Diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1, 99: 0})

diag_prog = defaultdict(int)
for _ in range(len(Diag_prog)):
    diag_prog[_] = Diag_prog[_]

SuperDone = False
Relative_base = 0
Done = False
OUTPUT = []


def Test(i):
    global SuperDone
    global Done
    global diag_prog
    global pointer
    global Relative_base
    instruction = str(diag_prog[i])
    #  print('instruction:', instruction)
    # print('diag_prog:',diag_prog[i],diag_prog[i+1],diag_prog[i+2] )
    op_code = int(instruction[-2:])
    # print(diag_prog)
    # print('instruction:', instruction)
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]
    logging.debug(' opcode: %s, modes: %s', op_code, modes)

    def Mode(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            return diag_prog[diag_prog[_pos + param + 1]]
        elif int(modes[param]) == 1:  # immediate mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[diag_prog[_pos + param + 1] + Relative_base]

    def ModeInput(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 1:  # immediate mode
            return _pos + param + 1
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[_pos + param + 1] + Relative_base

    def Add(_modes, pos):
        # print(' Adding:{} a {}'.format(Mode(_modes,pos,0), Mode(_modes,pos,1)))
        # print('Pred:',diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)
        # print('Po:',diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])
        # diag_prog[diag_prog[pos+3]]=Mode(_modes,pos,0) + Mode(_modes,pos,1)

    def Multiply(_modes, pos):
        # print(' Multiplying:{} a {}'.format(Mode(_modes,pos,0), Mode(_modes,pos,1)))
        # print('Pred:', diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)
        # print('Po:', diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])
        # diag_prog[diag_prog[pos+3]]=Mode(_modes,pos,0) * Mode(_modes,pos,1)

    def Jump_if_true(_modes, pos):
        if Mode(_modes, pos, 0):
            # print('{} is not zero, so jumping to  {}'.format(Mode(_modes,pos,0),Mode(_modes,pos,1)))
            return Mode(_modes, pos, 1)
        # print('{} is zero, so not jumping'.format(Mode(_modes,pos,0)))
        return None

    def Jump_if_false(_modes, pos):
        if not Mode(_modes, pos, 0):
            # print('{} is zero, so jumping to  {}'.format(Mode(_modes,pos,0),Mode(_modes,pos,1)))
            return Mode(_modes, pos, 1)
        # print('{} is not zero, so not jumping'.format(Mode(_modes,pos,0)))
        return None

    def Read(_modes, pos):
        global Relative_base
        if not int(_modes[0]):  # position mode
            # print(_pos+param+1,diag_prog[_pos+param+1])
            # print('Read input was stored',diag_prog[diag_prog[pos+1]-1],diag_prog[diag_prog[pos+1]],diag_prog[diag_prog[pos+1]+1] )
            diag_prog[diag_prog[pos + 1]] = int(input("What is the input:"))
            # print('Read input was stored',diag_prog[diag_prog[pos+1]-1],diag_prog[diag_prog[pos+1]],diag_prog[diag_prog[pos+1]+1] )
        elif int(_modes[0]) == 2:  # relative mode
            diag_prog[diag_prog[pos + 1] + Relative_base] = int(input("What is the input:"))

    def Write(_modes, pos):
        global SuperDone
        if not int(_modes[0]):  # position mode
            if diag_prog[pos + 1] not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[diag_prog[pos + 1]])
        elif int(_modes[0]) == 1:  # immediate mode
            if pos + 1 not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[pos + 1])
        elif int(_modes[0]) == 2:  # relative mode
            if diag_prog[pos + 1] + Relative_base not in set(diag_prog.keys()):
                SuperDone = True
            OUTPUT.append(diag_prog[diag_prog[pos + 1] + Relative_base])

    def Is_less_than(_modes, pos):
        # print('Pred porovnanim:', diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))
        # print(' P({} is less than {}), to position {}'.format(Mode(_modes,pos,0), Mode(_modes,pos,1), ModeInput(_modes,pos,2)))
        # print('Po porovnani:', diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])

    def Is_equal(_modes, pos):
        # print('Pred porovnanim:', diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))
        # print(' P({} equals {}) to position {}'.format(Mode(_modes,pos,0), Mode(_modes,pos,1), ModeInput(_modes,pos,2)))
        # print('Po porovnani:', diag_prog[ModeInput(_modes,pos,2)-1],diag_prog[ModeInput(_modes,pos,2)],diag_prog[ModeInput(_modes,pos,2)+1])

    def Adjust_relative_base(_modes, pos):
        global Relative_base
        Relative_base += Mode(_modes, pos, 0)
        logging.debug(' Adjusting relative base by %s to %s', Mode(_modes, pos, 0), Relative_base)

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        9: Adjust_relative_base,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 4) or (7 <= op_code <= 9):
        Opcode_dict[op_code](modes, i)
        return (number_of_par[op_code] + 1)
    else:
        Jump = Opcode_dict[op_code](modes, i)
        # print('Jump:', Jump)
        if Jump is None:
            return (number_of_par[op_code] + 1)
        else:
            return Jump - pointer


Done = False
pointer = 0
while not Done:
    posun = Test(pointer)
    pointer += posun
    # print('OUTPUT:', OUTPUT)
    # print('Relative_base:', Relative_base)
    # print('pointer:', pointer)
    # print('--------------------------')
    # input("Press Enter to continue...")
# print(OUTPUT)
