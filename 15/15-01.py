import logging

logging.basicConfig(filename='example.log', level=logging.DEBUG)
from collections import defaultdict
from copy import deepcopy as dc

"""At first execute 15-01.py and then copy variable Map into Show_map.py and run."""

open('example.log', 'w').close()
f = open('input15.txt', 'r')
Diag_prog = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1, 99: 0})

_diag_prog = defaultdict(int)
for _ in range(len(Diag_prog)):
    _diag_prog[_] = Diag_prog[_]

diag_prog = dc(_diag_prog)


def Test(try_direction, current_path, Init, i, diag_prog):
    global Done
    global pointer
    global Relative_base
    global OUTPUT
    global INPUT
    instruction = str(diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]

    def Mode(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            return diag_prog[diag_prog[_pos + param + 1]]
        elif int(modes[param]) == 1:  # immediate mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[diag_prog[_pos + param + 1] + Relative_base]

    def ModeInput(modes, _pos, param):
        global Relative_base
        if not int(modes[param]):  # position mode
            return diag_prog[_pos + param + 1]
        elif int(modes[param]) == 1:  # immediate mode
            return _pos + param + 1
        elif int(modes[param]) == 2:  # relative mode
            return diag_prog[_pos + param + 1] + Relative_base

    def Add(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) + Mode(_modes, pos, 1)

    def Multiply(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = Mode(_modes, pos, 0) * Mode(_modes, pos, 1)

    def Jump_if_true(_modes, pos):
        if Mode(_modes, pos, 0):
            return Mode(_modes, pos, 1)
        return None

    def Jump_if_false(_modes, pos):
        if not Mode(_modes, pos, 0):
            return Mode(_modes, pos, 1)
        return None

    def Read(_modes, pos):
        global Relative_base
        global OUTPUT
        global INPUT
        global end_of_path
        print('output: ', OUTPUT, 'input: ', INPUT)
        print('current_path: ', current_path)
        print('Position_of_robot: ', Position_of_robot)
        print('Done', Done)
        Update_map(Init)
        print('Map: ', Map)
        print('try_direction:', try_direction)
        print('Position_of_robot: ', Position_of_robot)
        print('Done', Done)
        if not Done:
            if current_path:
                go_direction = current_path.pop(0)
                if not int(_modes[0]):  # position mode
                    INPUT.append(go_direction)
                    print('went_direction', go_direction)
                    diag_prog[diag_prog[pos + 1]] = INPUT[-1]
                elif int(_modes[0]) == 2:  # relative mode
                    INPUT.append(go_direction)
                    diag_prog[diag_prog[pos + 1] + Relative_base] = INPUT[-1]
                    print('went_direction', go_direction)
            else:
                end_of_path = True
                if not int(_modes[0]):  # position mode
                    INPUT.append(try_direction)
                    diag_prog[diag_prog[pos + 1]] = INPUT[-1]
                elif int(_modes[0]) == 2:  # relative mode
                    INPUT.append(try_direction)
                    diag_prog[diag_prog[pos + 1] + Relative_base] = INPUT[-1]
        print('--------')

    def Write(_modes, pos):
        if not int(_modes[0]):  # position mode
            OUTPUT.append(diag_prog[diag_prog[pos + 1]])
        elif int(_modes[0]) == 1:  # immediate mode
            OUTPUT.append(diag_prog[pos + 1])
        elif int(_modes[0]) == 2:  # relative mode
            OUTPUT.append(diag_prog[diag_prog[pos + 1] + Relative_base])

    def Is_less_than(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) < Mode(_modes, pos, 1))

    def Is_equal(_modes, pos):
        diag_prog[ModeInput(_modes, pos, 2)] = int(Mode(_modes, pos, 0) == Mode(_modes, pos, 1))

    def Adjust_relative_base(_modes, pos):
        global Relative_base
        Relative_base += Mode(_modes, pos, 0)

    Opcode_dict = dict({1: Add,
                        2: Multiply,
                        3: Read,
                        4: Write,
                        5: Jump_if_true,
                        6: Jump_if_false,
                        7: Is_less_than,
                        8: Is_equal,
                        9: Adjust_relative_base,
                        99: None})

    if op_code == 99:
        Done = True
        return 0
    elif (1 <= op_code <= 4) or (7 <= op_code <= 9):
        Opcode_dict[op_code](modes, i)
        return (number_of_par[op_code] + 1)
    else:
        Jump = Opcode_dict[op_code](modes, i)
        if Jump is None:
            return (number_of_par[op_code] + 1)
        else:
            return Jump - pointer


# -----------------------------------------------------------------------------------------------------------------------

def Update_map(Init):
    global OUTPUT
    global INPUT
    global Position_of_robot
    global Found
    global Done
    if len(OUTPUT) > 0:
        if INPUT[-1] == 1:
            new_position = (Position_of_robot[0], Position_of_robot[1] + 1)
        elif INPUT[-1] == 2:
            new_position = (Position_of_robot[0], Position_of_robot[1] - 1)
        elif INPUT[-1] == 3:
            new_position = (Position_of_robot[0] - 1, Position_of_robot[1])
        elif INPUT[-1] == 4:
            new_position = (Position_of_robot[0] + 1, Position_of_robot[1])

        if OUTPUT[-1] == 0:
            Map[new_position] = '#'
            Done = True
        # elif OUTPUT[-1] == 1:
        #     Map[new_position]=Map[Position_of_robot]+1
        #     Position_of_robot=new_position
        #     if Init:
        #         print('Init',Init)
        #         Done=True
        #         Paths.append([try_direction])
        elif OUTPUT[-1] == 1:
            if new_position in Map.keys():
                if Map[new_position] < Map[Position_of_robot] + 1:
                    Done = True
                else:
                    Map[new_position] = Map[Position_of_robot] + 1
                    Position_of_robot = new_position
            else:
                Map[new_position] = Map[Position_of_robot] + 1
                Position_of_robot = new_position
            if Init:
                print('Init', Init)
                Done = True
                Paths.append([try_direction])
            elif end_of_path:
                Done = True
                if (_path[-1], try_direction) not in [(3, 4), (4, 3), (1, 2), (2, 1)]:
                    Paths.append(_path + [try_direction])


        elif OUTPUT[-1] == 2:
            Map[new_position] = Map[Position_of_robot] + 1
            Position_of_robot = new_position
            print('It was a long raod: ', Map[Position_of_robot], 'tiles.')
            if Init:
                Done = True
            Found = True


Map = dict()
Map[(0, 0)] = 0
Found = False


def Execute_program(try_direction, current_path=None, Init=False):
    global Relative_base
    global Done
    global OUTPUT
    global INPUT
    global pointer
    global _diag_prog
    global Position_of_robot
    Position_of_robot = (0, 0)
    diag_prog = dc(_diag_prog)
    Relative_base = 0
    Done = False
    OUTPUT = []
    INPUT = []
    pointer = 0
    while not Done:
        posun = Test(try_direction, current_path, Init, pointer, diag_prog)
        pointer += posun


Initialization = True
Paths = []
while (Initialization or Paths) and not Found:
    if Initialization:
        for try_direction in range(1, 5):
            Execute_program(try_direction, Init=True)
        Initialization = False
        print('----------------- Inicialization is done. -------------------')
    if Paths:
        print('--------------------- Let us do Paths -----------------------')
        print('Initialization', Initialization)
        print('Paths: ', Paths)
        _path = Paths.pop(0)
        for try_direction in range(1, 5):
            end_of_path = False
            path = dc(_path)
            print('Trying direction', try_direction, 'with path', path)
            Execute_program(try_direction, current_path=path)
