import cmath
import logging
logging.basicConfig(filename='example.log', level=logging.DEBUG)
open('example.log', 'w').close()

lines = []
with open('input10.txt', 'r') as f:
    while True:
        line = f.readline().strip()
        if not line:
            break
        lines.append(line.strip())

print(lines)

meteors = []
for j in range(len(lines)):
    for i in range(len(lines[0])):
        if lines[j][i] == '#':
            meteors.append(complex(i, j))

print(meteors)

matrix = []
for _ in range(len(meteors)):
    matrix.append([])

for k in range(len(meteors)):
    for n in range(len(meteors)):
        if k == n:
            matrix[k].append(None)
        else:
            matrix[k].append(cmath.phase(meteors[k]-meteors[n])/cmath.pi)

Visual = []
for meteor in range(len(matrix)):
    Visual.append(len(set(matrix[meteor]))-1)


for meteor in range(len(matrix)):
    print(matrix[meteor])

print(Visual)
print(max(Visual))
