import logging
import cmath
from collections import defaultdict

logging.basicConfig(filename='example.log', level=logging.DEBUG)
open('example.log', 'w').close()


lines = []
with open('input10.txt', 'r') as f:
    while True:
        line = f.readline().strip()
        if not line:
            break
        lines.append(line.strip())

# print(lines)

meteors = []
for j in range(len(lines)):
    for i in range(len(lines[0])):
        if lines[j][i] == '#':
            meteors.append(complex(i, j))


# print(meteors)

def get_station():
    global meteors
    matrix = []
    for _ in range(len(meteors)):
        matrix.append([])
    for k in range(len(meteors)):
        for n in range(len(meteors)):
            if k == n:
                matrix[k].append(None)
            else:
                matrix[k].append(cmath.phase(meteors[k] - meteors[n]) / cmath.pi)
    Visual = []
    for meteor in range(len(matrix)):
        Visual.append(len(set(matrix[meteor])) - 1)
    # print(Visual)
    _Max = 0
    _position = None
    _value = 0
    for value in range(len(Visual)):
        if Visual[value] > _Max:
            _Max = Visual[value]
            _position = meteors[value]
            _value = value
    return _value, _Max, _position


def get_sights(value):
    _sights = defaultdict(list)
    for _ in range(len(meteors)):
        if value != _:
            _sights[cmath.phase((-meteors[value] + meteors[_])) / cmath.pi].append(meteors[_])
    # print(_sights)
    for sight in _sights.keys():
        _sights[sight].sort(key=lambda x: abs(-meteors[value] + x), reverse=False)
    sights_ = dict()
    # print(_sights)
    for k in _sights.keys():
        if k < -0.5:
            sights_[k + 2.5] = _sights[k]
        else:
            sights_[k + 0.5] = _sights[k]
    return sights_


st_on_meteor_no, Max, our_station = get_station()

sights = get_sights(st_on_meteor_no)

shoted_list = []
while sights:
    for key in sorted(sights.keys()):
        shoted = sights[key].pop(0)
        shoted_list.append(shoted)
        if len(sights[key]) == 0:
            sights.pop(key)

# print(shoted_list)
print(int(100 * shoted_list[199].real + shoted_list[199].imag))
