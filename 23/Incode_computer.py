from collections import defaultdict

f = open('input23.txt', 'r')
diag_prog_list = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 3, 2: 3, 3: 1, 4: 1, 5: 2, 6: 2, 7: 3, 8: 3, 9: 1, 99: 0})

diag_prog = defaultdict(int)
for _ in range(len(diag_prog_list)):
    diag_prog[_] = diag_prog_list[_]

OUTPUT = []


def process(i, _diag_prog, _relative_base):
    instruction = str(_diag_prog[i])
    op_code = int(instruction[-2:])
    modes = instruction[:-2]
    doplnok = number_of_par[op_code] - len(modes)
    if doplnok > 0:
        for _ in range(doplnok):
            modes = '0' + modes
    modes = modes[::-1]

    def mode(modes_, _pos, param, relative_base_mode):
        if not int(modes_[param]):  # position mode
            return _diag_prog[_diag_prog[_pos + param + 1]]
        elif int(modes_[param]) == 1:  # immediate mode
            return _diag_prog[_pos + param + 1]
        elif int(modes_[param]) == 2:  # relative mode
            return _diag_prog[_diag_prog[_pos + param + 1] + relative_base_mode]

    def mode_input(modes_, _pos, param, relative_base_mode_input):
        if not int(modes_[param]):  # position mode
            return _diag_prog[_pos + param + 1]
        elif int(modes_[param]) == 1:  # immediate mode
            return _pos + param + 1
        elif int(modes_[param]) == 2:  # relative mode
            return _diag_prog[_pos + param + 1] + relative_base_mode_input

    def add(_modes, pos, relative_base_add):
        _diag_prog[mode_input(_modes, pos, 2, relative_base_add)] = mode(_modes, pos, 0, relative_base_add) + mode(
            _modes, pos, 1, relative_base_add)

    def multiply(_modes, pos, relative_base_multiply):
        _diag_prog[mode_input(_modes, pos, 2, relative_base_multiply)] = mode(_modes, pos, 0,
                                                                              relative_base_multiply) * mode(_modes,
                                                                                                             pos, 1,
                                                                                                             relative_base_multiply)

    def jump_if_true(_modes, pos, relative_base_jump_if_true):
        if mode(_modes, pos, 0, relative_base_jump_if_true):
            return mode(_modes, pos, 1, relative_base_jump_if_true)
        return None

    def jump_if_false(_modes, pos, relative_base_jump_if_false):
        if not mode(_modes, pos, 0, relative_base_jump_if_false):
            return mode(_modes, pos, 1, relative_base_jump_if_false)
        return None

    def read(_modes, pos, relative_base_read):
        if not int(_modes[0]):  # position mode
            _diag_prog[_diag_prog[pos + 1]] = int(input("What is the input:"))
        elif int(_modes[0]) == 2:  # relative mode
            _diag_prog[_diag_prog[pos + 1] + relative_base_read] = int(input("What is the input:"))

    def write(_modes, pos, relative_base_write):
        if not int(_modes[0]):  # position mode
            OUTPUT.append(_diag_prog[_diag_prog[pos + 1]])
        elif int(_modes[0]) == 1:  # immediate mode
            OUTPUT.append(_diag_prog[pos + 1])
        elif int(_modes[0]) == 2:  # relative mode
            OUTPUT.append(_diag_prog[_diag_prog[pos + 1] + relative_base_write])

    def is_less_than(_modes, pos, relative_base_is_less):
        _diag_prog[mode_input(_modes, pos, 2, relative_base_is_less)] = int(
            mode(_modes, pos, 0, relative_base_is_less) < mode(_modes, pos, 1, relative_base_is_less))

    def is_equal(_modes, pos, relative_base_is_equal):
        _diag_prog[mode_input(_modes, pos, 2, relative_base_is_equal)] = int(
            mode(_modes, pos, 0, relative_base_is_equal) == mode(_modes, pos, 1, relative_base_is_equal))

    def adjust_relative_base(_modes, pos, relative_base_):
        relative_base_ += mode(_modes, pos, 0, relative_base_)
        return relative_base_

    Opcode_dict = dict({1: add,
                        2: multiply,
                        3: read,
                        4: write,
                        5: jump_if_true,
                        6: jump_if_false,
                        7: is_less_than,
                        8: is_equal,
                        9: adjust_relative_base,
                        99: None})

    if op_code == 99:
        return 0, True, _diag_prog, _relative_base
    elif (1 <= op_code <= 2) or (7 <= op_code <= 8):
        Opcode_dict[op_code](modes, i, _relative_base)
        return (number_of_par[op_code] + 1), False, _diag_prog, _relative_base
    elif 3 <= op_code <= 4:
        Opcode_dict[op_code](modes, i, _relative_base)
        return (number_of_par[op_code] + 1), False, _diag_prog, _relative_base
    elif op_code == 9:
        _relative_base = Opcode_dict[op_code](modes, i, _relative_base)
        return (number_of_par[op_code] + 1), False, _diag_prog, _relative_base
    else:
        Jump = Opcode_dict[op_code](modes, i, _relative_base)
        if Jump is None:
            return (number_of_par[op_code] + 1), False, _diag_prog, _relative_base
        else:
            return (Jump - i), False, _diag_prog, _relative_base


relative_base = 0
done = False
pointer = 0
while not done:
    movement, done, diag_prog, relative_base = process(pointer, diag_prog, relative_base)
    pointer += movement
    print(OUTPUT)
