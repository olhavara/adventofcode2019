import logging
from collections import defaultdict
from queue import Empty, Queue
from threading import Thread

logging.basicConfig(filename='example.log', level=logging.DEBUG)

open('example.log', 'w').close()
f = open('input23.txt', 'r')
diag_prog_list = list(map(int, f.readline().strip().split(',')))
number_of_par = dict({1: 4, 2: 4, 3: 2, 4: 2, 5: 3, 6: 3, 7: 4, 8: 4, 9: 2})


def mode(diag_prog, modes_, _pos, param, relative_base_mode):
            if not int(modes_[param]):  # position mode
                return diag_prog[diag_prog[_pos + param + 1]]
            elif int(modes_[param]) == 1:  # immediate mode
                return diag_prog[_pos + param + 1]
            elif int(modes_[param]) == 2:  # relative mode
                return diag_prog[diag_prog[_pos + param + 1] + relative_base_mode]


def mode_input(diag_prog, modes_, _pos, param, relative_base_mode_input):
    if not int(modes_[param]):  # position mode
        return diag_prog[_pos + param + 1]
    elif int(modes_[param]) == 1:  # immediate mode
        return _pos + param + 1
    elif int(modes_[param]) == 2:  # relative mode
        return diag_prog[_pos + param + 1] + relative_base_mode_input


def incode_computer(address, diag_prog, pipeline_in, nat_q):
    logging.info("Incode_computer number {} is starting".format(address))
    diag = defaultdict(int)
    leng = len(diag_prog)
    for _, n in enumerate(diag_prog):
        diag[_] = n
    diag_prog = diag
    relative_base = 0
    pointer = 0
    while pointer < leng:
        instruction = str(diag_prog[pointer])
        op_code = int(instruction[-2:])
        modes = instruction[:-2]
        doplnok = number_of_par[op_code] - len(modes) - 1
        if doplnok > 0:
            for _ in range(doplnok):
                modes = '0' + modes
        modes = modes[::-1]

        if op_code == 99:
            break
        elif op_code == 1:
            diag_prog[mode_input(diag_prog, modes, pointer, 2, relative_base)] = mode(diag_prog, modes, pointer, 0,
                                                                           relative_base) + mode(diag_prog, modes, pointer, 1,
                                                                                                 relative_base)
        elif op_code == 2:
            diag_prog[mode_input(diag_prog, modes, pointer, 2, relative_base)] = mode(diag_prog, modes, pointer, 0,
                                                                           relative_base) * mode(diag_prog, modes, pointer, 1,
                                                                                                 relative_base)
        elif op_code == 3:
            try:
                curr_inp = pipeline_in.get(timeout=0.1)
            except Empty:
                curr_inp = -1
            if curr_inp == 'quit':
                return
            idle = curr_inp == -1
            if idle:
                nat_q.put(('idle', address))
            else:
                nat_q.put(('active', address))
            if not int(modes[0]):  # position mode
                 diag_prog[diag_prog[pointer + 1]] = curr_inp
            elif int(modes[0]) == 2:  # relative mode
                    diag_prog[diag_prog[pointer + 1] + relative_base] = curr_inp
        elif op_code == 4:
            if not int(modes[0]):  # position mode
                curr_out = diag_prog[diag_prog[pointer + 1]]
            elif int(modes[0]) == 1:  # immediate mode
                curr_out = diag_prog[pointer + 1]
            elif int(modes[0]) == 2:  # relative mode
                curr_out = diag_prog[diag_prog[pointer + 1] + relative_base]
            yield curr_out
        elif op_code == 5:
            if mode(diag_prog, modes, pointer, 0, relative_base):
                pointer = mode(diag_prog, modes, pointer, 1, relative_base)
                continue
        elif op_code == 6:
            if not mode(diag_prog, modes, pointer, 0, relative_base):  # mode() == 0
                pointer = mode(diag_prog, modes, pointer, 1, relative_base)
                continue
        elif op_code == 7:
            diag_prog[mode_input(diag_prog, modes, pointer, 2, relative_base)] = int(
                mode(diag_prog, modes, pointer, 0, relative_base) < mode(diag_prog, modes, pointer, 1, relative_base))
        elif op_code == 8:
            diag_prog[mode_input(diag_prog, modes, pointer, 2, relative_base)] = int(
                mode(diag_prog, modes, pointer, 0, relative_base) == mode(diag_prog, modes, pointer, 1, relative_base))
        elif op_code == 9:
            relative_base += mode(diag_prog, modes, pointer, 0, relative_base)
        pointer += number_of_par[op_code]


def run_pipe(i, _diag_prog_list, pipeline_in, pipelines, nat_q):
    outs = []
    for out in incode_computer(i, _diag_prog_list, pipeline_in, nat_q):
        outs.append(out)
        if len(outs) % 3 == 0:
            if outs[-3] == 255:
                nat_q.put(('packet', (outs[-2], outs[-1])))
            else:
                pipelines[outs[-3]].put(outs[-2])
                pipelines[outs[-3]].put(outs[-1])


def run_nat1(nat_q):
    while True:
        cmd, v = nat_q.get()
        if cmd == 'packet':
            x, y = v
            return y


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    pipelines = [Queue() for _ in range(50)]
    for j in range(50):
        pipelines[j].put(j)
    nat_q = Queue()
    threads = []
    for i in range(50):
        threads.append(Thread(target=run_pipe, args=(i, diag_prog_list, pipelines[i], pipelines, nat_q)))
    for t in threads:
        t.start()
    Y = run_nat1(nat_q)
    for pipeline in pipelines:
        pipeline.put('quit')
    print(Y)

