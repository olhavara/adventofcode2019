start = []
with open('input24.txt', 'r') as f:
    while True:
        line = f.readline().strip()
        if not line:
            break
        start.append(line)

def iterate(state):
    new_state = []
    for row in range(5):
        new_line = ''
        for col in range(5):
            bugs_around = 0
            if row != 0:
                if state[row-1][col] == '#':
                    bugs_around += 1
            if row != 4:
                if state[row+1][col] == '#':
                    bugs_around += 1
            if col != 4:
                if state[row][col+1] == '#':
                    bugs_around += 1
            if col != 0:
                if state[row][col-1] == '#':
                    bugs_around += 1
            if state[row][col] == '#' and bugs_around != 1:
                new_place = '.'
            elif state[row][col] == '.' and bugs_around in {1, 2}:
                new_place = '#'
            else:
                new_place = state[row][col]
            new_line += new_place
        new_state.append(new_line)
    return new_state


def print_state(state):
    for _line in state:
        print(_line)

def translate(state):
    start_bin = ''
    for char in reversed(''.join(state)):
        start_bin += str(int(char == '#'))
    start_dec = int(start_bin,2)
    return start_dec


past_states = set()
past_states.add(translate(start))

while True:
    new_state = iterate(start)
    new_code = translate(new_state)
    if new_code in past_states:
        print_state(new_state)
        print(new_code, len(past_states))
        break
    else:
        past_states.add(new_code)
    start = new_state



