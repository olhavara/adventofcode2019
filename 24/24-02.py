from collections import deque
situation = deque()
start = []
with open('input24.txt', 'r') as f:
    while True:
        line = f.readline().strip()
        if not line:
            break
        start.append(line)


start[2] = start[2][:2] + '?' + start[2][3:]
situation.append(start)

NEW_LEVEL = ['.....', '.....', '..?..', '.....', '.....']
situation.append(NEW_LEVEL)
situation.appendleft(NEW_LEVEL)


INSIDE_NEIGHBOURS = set()
NOT_NEIGHBOURS = {(1, 1), (1, 3), (3, 1), (3, 3)}
OUTSIDE_NEIGHBOURS = set()
for row in range(5):
    for col in range(5):
        if 1 <= row <= 3 and 1 <= col <= 3:
            INSIDE_NEIGHBOURS.add((row, col))
        else:
            OUTSIDE_NEIGHBOURS.add((row, col))
INSIDE_NEIGHBOURS.discard((2, 2))
INSIDE_NEIGHBOURS = INSIDE_NEIGHBOURS.difference(NOT_NEIGHBOURS)

print(INSIDE_NEIGHBOURS)
print(NOT_NEIGHBOURS)
print(OUTSIDE_NEIGHBOURS)


def count_neighbour_bugs(situation, level, row, col):
    bugs_around = 0
    if (row, col) in NOT_NEIGHBOURS:
        if row != 0:
            if situation[level][row-1][col] == '#':
                bugs_around += 1
        if row != 4:
            if situation[level][row+1][col] == '#':
                bugs_around += 1
        if col != 4:
            if situation[level][row][col+1] == '#':
                bugs_around += 1
        if col != 0:
            if situation[level][row][col-1] == '#':
                bugs_around += 1
    if (row, col) in INSIDE_NEIGHBOURS:
        if row != 3:
            if situation[level][row-1][col] == '#':
                bugs_around += 1
        if row != 1:
            if situation[level][row+1][col] == '#':
                bugs_around += 1
        if col != 1:
            if situation[level][row][col+1] == '#':
                bugs_around += 1
        if col != 3:
            if situation[level][row][col-1] == '#':
                bugs_around += 1
        if level != 0:
            if row == 3:
                bugs_around += situation[level-1][4].count('#')
            if row == 1:
                bugs_around += situation[level-1][0].count('#')
            if col == 1:
                for ro in range(5):
                    if situation[level-1][ro][0] == '#':
                        bugs_around += 1
            if col == 3:
                for ro in range(5):
                    if situation[level-1][ro][4] == '#':
                        bugs_around += 1
    if (row, col) in OUTSIDE_NEIGHBOURS:
        if row != 0:
            if situation[level][row-1][col] == '#':
                bugs_around += 1
        if row != 4:
            if situation[level][row+1][col] == '#':
                bugs_around += 1
        if col != 4:
            if situation[level][row][col+1] == '#':
                bugs_around += 1
        if col != 0:
            if situation[level][row][col-1] == '#':
                bugs_around += 1
        if level != (len(situation) - 1):
            if row == 0 and situation[level+1][1][2] == '#':
                bugs_around += 1
            if row == 4 and situation[level+1][3][2] == '#':
                bugs_around += 1
            if col == 0 and situation[level+1][2][1] == '#':
                bugs_around += 1
            if col == 4 and situation[level+1][2][3] == '#':
                bugs_around += 1
    return bugs_around


def iterate(situation):
    new_situation = deque()
    for level in range(len(situation)):
        new_state = []
        for row in range(5):
            new_line = ''
            for col in range(5):
                bugs_around = count_neighbour_bugs(situation, level, row, col)
                if situation[level][row][col] == '#' and bugs_around != 1:
                    new_place = '.'
                elif situation[level][row][col] == '.' and bugs_around in {1, 2}:
                    new_place = '#'
                else:
                    new_place = situation[level][row][col]
                new_line += new_place
            new_state.append(new_line)
        new_situation.append(new_state)
    new_inside = False
    new_outside = False
    for (row, col) in INSIDE_NEIGHBOURS:
        new_inside = new_inside or (new_situation[0][row][col] == '#')
    for (row, col) in OUTSIDE_NEIGHBOURS:
        new_outside = new_outside or (new_situation[len(new_situation)-1][row][col] == '#')
    if new_inside:
        new_situation.appendleft(NEW_LEVEL)
    if new_outside:
        new_situation.append(NEW_LEVEL)
    return new_situation


def print_state(state):
    for _line in state:
        print(_line)

def count_bugs(situation):
    bugs = 0
    for level in range(len(situation)):
        for row in range(5):
            for col in range(5):
                if situation[level][row][col] == '#':
                    bugs += 1
    print('There are {} bugs'.format(bugs))

def print_situation(situation):
    for level, _state in enumerate(situation):
        print('level:', level)
        print_state(_state)
        print()

def translate(state):
    start_bin = ''
    for char in reversed(''.join(state)):
        start_bin += str(int(char == '#'))
    start_dec = int(start_bin,2)
    return start_dec

for _ in range(200):
    situation = iterate(situation)

print_situation(situation)
count_bugs(situation)





